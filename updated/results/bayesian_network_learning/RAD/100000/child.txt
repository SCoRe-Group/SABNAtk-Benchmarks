2022-07-01 14:54:08 INFO input: my_data/100000/child.csv
2022-07-01 14:54:08 INFO variables: 20
2022-07-01 14:54:08 INFO instances: 100000
2022-07-01 14:54:08 INFO function: 
2022-07-01 14:54:08 INFO limit: 6
2022-07-01 14:54:08 INFO priors: 
2022-07-01 14:54:08 INFO output: tmp.mps
2022-07-01 14:54:08 INFO searching parent sets
2022-07-01 14:54:08 DEBUG processing with N = 1
2022-07-01 14:54:08 DEBUG building RadCounter engine
2022-07-01 14:54:08 DEBUG engine ready in 14825ms
2022-07-01 14:54:08 DEBUG building MDLEngine...
2022-07-01 14:54:08 DEBUG MDLEngine getting entropies...
2022-07-01 14:54:08 DEBUG variables reordered for efficiency
2022-07-01 14:54:08 INFO maximal number of parents limited to 6
2022-07-01 14:54:08 DEBUG MDLEngine ready!
2022-07-01 14:54:08 DEBUG updating MPS...
2022-07-01 14:54:08 DEBUG sort done, MPS update size is 20
2022-07-01 14:54:08 DEBUG MPS updated
2022-07-01 14:54:08 INFO processed layer 0, size of next layer 20, time: 3.2125e-05s (0h0m0s)
2022-07-01 14:54:08 DEBUG memory required: 320B
2022-07-01 14:54:08 DEBUG updating MPS...
2022-07-01 14:54:08 DEBUG sort done, MPS update size is 348
2022-07-01 14:54:08 DEBUG MPS updated
2022-07-01 14:54:08 INFO processed layer 1, size of next layer 190, time: 0.19909s (0h0m0s)
2022-07-01 14:54:08 DEBUG memory required: 2.969KB
2022-07-01 14:54:11 DEBUG updating MPS...
2022-07-01 14:54:11 DEBUG sort done, MPS update size is 1984
2022-07-01 14:54:11 DEBUG MPS updated
2022-07-01 14:54:11 INFO processed layer 2, size of next layer 1140, time: 2.56944s (0h0m2s)
2022-07-01 14:54:11 DEBUG memory required: 17.81KB
2022-07-01 14:54:30 DEBUG updating MPS...
2022-07-01 14:54:30 DEBUG sort done, MPS update size is 4630
2022-07-01 14:54:30 DEBUG MPS updated
2022-07-01 14:54:30 INFO processed layer 3, size of next layer 4845, time: 19.2345s (0h0m19s)
2022-07-01 14:54:30 DEBUG memory required: 75.7KB
2022-07-01 14:56:06 DEBUG updating MPS...
2022-07-01 14:56:06 DEBUG sort done, MPS update size is 4352
2022-07-01 14:56:06 DEBUG MPS updated
2022-07-01 14:56:06 INFO processed layer 4, size of next layer 15504, time: 96.241s (0h1m36s)
2022-07-01 14:56:06 DEBUG memory required: 242.2KB
2022-07-01 15:01:58 DEBUG updating MPS...
2022-07-01 15:01:58 DEBUG sort done, MPS update size is 858
2022-07-01 15:01:58 DEBUG MPS updated
2022-07-01 15:01:58 INFO processed layer 5, size of next layer 38760, time: 352.086s (0h5m52s)
2022-07-01 15:01:58 DEBUG memory required: 605.6KB
2022-07-01 15:18:09 DEBUG updating MPS...
2022-07-01 15:18:09 DEBUG sort done, MPS update size is 7
2022-07-01 15:18:09 DEBUG MPS updated
2022-07-01 15:18:09 INFO processed layer 6, size of next layer 0, time: 970.999s (0h16m10s)
2022-07-01 15:18:09 DEBUG memory required: 0B
2022-07-01 15:18:09 INFO writing MPS with 12199 parent sets
2022-07-01 15:18:09 INFO searching done in 1441.73s (0h24m1s)
