2022-07-03 16:16:01 INFO input: my_data/10000/child.csv
2022-07-03 16:16:01 INFO variables: 20
2022-07-03 16:16:01 INFO instances: 10000
2022-07-03 16:16:01 INFO function: 
2022-07-03 16:16:01 INFO limit: 6
2022-07-03 16:16:01 INFO priors: 
2022-07-03 16:16:01 INFO output: tmp.mps
2022-07-03 16:16:01 INFO searching parent sets
2022-07-03 16:16:01 DEBUG processing with N = 1
2022-07-03 16:16:01 DEBUG building CTCounter engine
2022-07-03 16:16:01 DEBUG engine ready in 643ms
2022-07-03 16:16:01 DEBUG building MDLEngine...
2022-07-03 16:16:01 DEBUG MDLEngine getting entropies...
2022-07-03 16:17:39 DEBUG variables reordered for efficiency
2022-07-03 16:17:39 INFO maximal number of parents limited to 6
2022-07-03 16:17:39 DEBUG MDLEngine ready!
2022-07-03 16:17:39 DEBUG updating MPS...
2022-07-03 16:17:39 DEBUG sort done, MPS update size is 20
2022-07-03 16:17:39 DEBUG MPS updated
2022-07-03 16:17:39 INFO processed layer 0, size of next layer 20, time: 0.000229537s (0h0m0s)
2022-07-03 16:17:39 DEBUG memory required: 320B
2022-07-03 16:17:39 DEBUG updating MPS...
2022-07-03 16:17:39 DEBUG sort done, MPS update size is 280
2022-07-03 16:17:39 DEBUG MPS updated
2022-07-03 16:17:39 INFO processed layer 1, size of next layer 190, time: 0.00428533s (0h0m0s)
2022-07-03 16:17:39 DEBUG memory required: 2.969KB
2022-07-03 16:17:39 DEBUG updating MPS...
2022-07-03 16:17:39 DEBUG sort done, MPS update size is 853
2022-07-03 16:17:39 DEBUG MPS updated
2022-07-03 16:17:39 INFO processed layer 2, size of next layer 1140, time: 0.0426983s (0h0m0s)
2022-07-03 16:17:39 DEBUG memory required: 17.81KB
2022-07-03 16:17:39 DEBUG updating MPS...
2022-07-03 16:17:39 DEBUG sort done, MPS update size is 696
2022-07-03 16:17:39 DEBUG MPS updated
2022-07-03 16:17:39 INFO processed layer 3, size of next layer 4845, time: 0.280493s (0h0m0s)
2022-07-03 16:17:39 DEBUG memory required: 75.7KB
2022-07-03 16:17:40 DEBUG updating MPS...
2022-07-03 16:17:40 DEBUG sort done, MPS update size is 35
2022-07-03 16:17:40 DEBUG MPS updated
2022-07-03 16:17:40 INFO processed layer 4, size of next layer 15504, time: 1.36798s (0h0m1s)
2022-07-03 16:17:40 DEBUG memory required: 242.2KB
2022-07-03 16:17:46 DEBUG updating MPS...
2022-07-03 16:17:46 DEBUG sort done, MPS update size is 0
2022-07-03 16:17:46 DEBUG MPS updated
2022-07-03 16:17:46 INFO processed layer 5, size of next layer 37924, time: 5.33953s (0h0m5s)
2022-07-03 16:17:46 DEBUG memory required: 592.6KB
2022-07-03 16:18:01 DEBUG updating MPS...
2022-07-03 16:18:01 DEBUG sort done, MPS update size is 0
2022-07-03 16:18:01 DEBUG MPS updated
2022-07-03 16:18:01 INFO processed layer 6, size of next layer 0, time: 14.7643s (0h0m14s)
2022-07-03 16:18:01 DEBUG memory required: 0B
2022-07-03 16:18:01 INFO writing MPS with 1884 parent sets
2022-07-03 16:18:01 INFO searching done in 119.894s (0h1m59s)
