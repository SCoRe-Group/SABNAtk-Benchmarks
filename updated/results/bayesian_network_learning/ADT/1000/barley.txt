2022-07-09 23:40:21 INFO input: my_data/1000/barley.csv
2022-07-09 23:40:21 INFO variables: 48
2022-07-09 23:40:21 INFO instances: 1000
2022-07-09 23:40:21 INFO function: 
2022-07-09 23:40:21 INFO limit: 6
2022-07-09 23:40:21 INFO priors: 
2022-07-09 23:40:21 INFO output: tmp.mps
2022-07-09 23:40:21 INFO searching parent sets
2022-07-09 23:40:21 DEBUG processing with N = 1
2022-07-09 23:40:21 DEBUG building ADTree engine, prepare for impact...
2022-07-09 23:40:22 DEBUG engine ready in 1007187ms
2022-07-09 23:40:22 DEBUG building MDLEngine...
2022-07-09 23:40:22 DEBUG MDLEngine getting entropies...
2022-07-09 23:47:04 INFO maximal number of parents limited to 6
2022-07-09 23:47:04 DEBUG MDLEngine ready!
2022-07-09 23:47:04 DEBUG updating MPS...
2022-07-09 23:47:04 DEBUG sort done, MPS update size is 48
2022-07-09 23:47:04 DEBUG MPS updated
2022-07-09 23:47:04 INFO processed layer 0, size of next layer 48, time: 0.000165891s (0h0m0s)
2022-07-09 23:47:04 DEBUG memory required: 768B
2022-07-09 23:47:04 DEBUG updating MPS...
2022-07-09 23:47:04 DEBUG sort done, MPS update size is 2256
2022-07-09 23:47:04 DEBUG MPS updated
2022-07-09 23:47:04 INFO processed layer 1, size of next layer 1128, time: 0.0300912s (0h0m0s)
2022-07-09 23:47:04 DEBUG memory required: 17.62KB
2022-07-09 23:47:08 DEBUG updating MPS...
2022-07-09 23:47:08 DEBUG sort done, MPS update size is 51814
2022-07-09 23:47:08 DEBUG MPS updated
2022-07-09 23:47:08 INFO processed layer 2, size of next layer 17296, time: 3.42564s (0h0m3s)
2022-07-09 23:47:08 DEBUG memory required: 270.2KB
2022-07-09 23:49:54 DEBUG updating MPS...
2022-07-09 23:49:54 DEBUG sort done, MPS update size is 771167
2022-07-09 23:49:55 DEBUG MPS updated
2022-07-09 23:49:55 INFO processed layer 3, size of next layer 194580, time: 167.501s (0h2m47s)
2022-07-09 23:49:55 DEBUG memory required: 2.969MB
2022-07-10 01:05:59 DEBUG updating MPS...
2022-07-10 01:05:59 DEBUG sort done, MPS update size is 8198194
2022-07-10 01:08:32 DEBUG MPS updated
2022-07-10 01:08:32 INFO processed layer 4, size of next layer 1712304, time: 4716.93s (1h18m36s)
2022-07-10 01:08:32 DEBUG memory required: 26.13MB
