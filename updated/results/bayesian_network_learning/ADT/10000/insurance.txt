2022-07-10 15:00:10 INFO input: my_data/10000/insurance.csv
2022-07-10 15:00:10 INFO variables: 27
2022-07-10 15:00:10 INFO instances: 10000
2022-07-10 15:00:10 INFO function: 
2022-07-10 15:00:10 INFO limit: 6
2022-07-10 15:00:10 INFO priors: 
2022-07-10 15:00:10 INFO output: tmp.mps
2022-07-10 15:00:10 INFO searching parent sets
2022-07-10 15:00:10 DEBUG processing with N = 1
2022-07-10 15:00:10 DEBUG building ADTree engine, prepare for impact...
2022-07-10 15:00:15 DEBUG engine ready in 4727876ms
2022-07-10 15:00:15 DEBUG building MDLEngine...
2022-07-10 15:00:15 DEBUG MDLEngine getting entropies...
2022-07-10 15:01:05 INFO maximal number of parents limited to 6
2022-07-10 15:01:05 DEBUG MDLEngine ready!
2022-07-10 15:01:05 DEBUG updating MPS...
2022-07-10 15:01:05 DEBUG sort done, MPS update size is 27
2022-07-10 15:01:05 DEBUG MPS updated
2022-07-10 15:01:05 INFO processed layer 0, size of next layer 27, time: 7.1243e-05s (0h0m0s)
2022-07-10 15:01:05 DEBUG memory required: 432B
2022-07-10 15:01:05 DEBUG updating MPS...
2022-07-10 15:01:05 DEBUG sort done, MPS update size is 702
2022-07-10 15:01:05 DEBUG MPS updated
2022-07-10 15:01:05 INFO processed layer 1, size of next layer 351, time: 0.00238716s (0h0m0s)
2022-07-10 15:01:05 DEBUG memory required: 5.484KB
2022-07-10 15:01:05 DEBUG updating MPS...
2022-07-10 15:01:05 DEBUG sort done, MPS update size is 8773
2022-07-10 15:01:05 DEBUG MPS updated
2022-07-10 15:01:05 INFO processed layer 2, size of next layer 2925, time: 0.0870087s (0h0m0s)
2022-07-10 15:01:05 DEBUG memory required: 45.7KB
2022-07-10 15:01:07 DEBUG updating MPS...
2022-07-10 15:01:07 DEBUG sort done, MPS update size is 70152
2022-07-10 15:01:07 DEBUG MPS updated
2022-07-10 15:01:07 INFO processed layer 3, size of next layer 17550, time: 1.91994s (0h0m1s)
2022-07-10 15:01:07 DEBUG memory required: 274.2KB
2022-07-10 15:01:35 DEBUG updating MPS...
2022-07-10 15:01:35 DEBUG sort done, MPS update size is 403094
2022-07-10 15:01:36 DEBUG MPS updated
2022-07-10 15:01:36 INFO processed layer 4, size of next layer 80730, time: 28.7842s (0h0m28s)
2022-07-10 15:01:36 DEBUG memory required: 1.232MB
2022-07-10 15:06:35 DEBUG updating MPS...
2022-07-10 15:06:35 DEBUG sort done, MPS update size is 1771908
2022-07-10 15:06:44 DEBUG MPS updated
2022-07-10 15:06:44 INFO processed layer 5, size of next layer 296010, time: 308.584s (0h5m8s)
2022-07-10 15:06:44 DEBUG memory required: 4.517MB
2022-07-10 15:45:07 DEBUG updating MPS...
2022-07-10 15:45:07 DEBUG sort done, MPS update size is 6193520
2022-07-10 15:49:45 DEBUG MPS updated
2022-07-10 15:49:45 INFO processed layer 6, size of next layer 0, time: 2580.64s (0h43m0s)
2022-07-10 15:49:45 DEBUG memory required: 0B
2022-07-10 15:49:45 INFO writing MPS with 8448176 parent sets
2022-07-10 15:49:53 INFO searching done in 2982.92s (0h49m42s)
