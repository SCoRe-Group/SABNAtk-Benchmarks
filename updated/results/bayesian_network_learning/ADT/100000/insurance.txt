2022-07-20 22:58:50 INFO input: my_data/100000/insurance.csv
2022-07-20 22:58:50 INFO variables: 27
2022-07-20 22:58:50 INFO instances: 100000
2022-07-20 22:58:50 INFO function: 
2022-07-20 22:58:50 INFO limit: 6
2022-07-20 22:58:50 INFO priors: 
2022-07-20 22:58:50 INFO output: tmp.mps
2022-07-20 22:58:50 INFO searching parent sets
2022-07-20 22:58:50 DEBUG processing with N = 1
2022-07-20 22:58:50 DEBUG building ADTree engine, prepare for impact...
2022-07-20 23:04:36 DEBUG engine ready in 345785974ms
2022-07-20 23:04:36 DEBUG building MDLEngine...
2022-07-20 23:04:36 DEBUG MDLEngine getting entropies...
2022-07-20 23:10:49 INFO maximal number of parents limited to 6
2022-07-20 23:10:49 DEBUG MDLEngine ready!
2022-07-20 23:10:49 DEBUG updating MPS...
2022-07-20 23:10:49 DEBUG sort done, MPS update size is 27
2022-07-20 23:10:49 DEBUG MPS updated
2022-07-20 23:10:49 INFO processed layer 0, size of next layer 27, time: 6.7811e-05s (0h0m0s)
2022-07-20 23:10:49 DEBUG memory required: 432B
2022-07-20 23:10:49 DEBUG updating MPS...
2022-07-20 23:10:49 DEBUG sort done, MPS update size is 702
2022-07-20 23:10:49 DEBUG MPS updated
2022-07-20 23:10:49 INFO processed layer 1, size of next layer 351, time: 0.00246142s (0h0m0s)
2022-07-20 23:10:49 DEBUG memory required: 5.484KB
2022-07-20 23:10:49 DEBUG updating MPS...
2022-07-20 23:10:49 DEBUG sort done, MPS update size is 8773
2022-07-20 23:10:49 DEBUG MPS updated
2022-07-20 23:10:49 INFO processed layer 2, size of next layer 2925, time: 0.0909367s (0h0m0s)
2022-07-20 23:10:49 DEBUG memory required: 45.7KB
2022-07-20 23:10:51 DEBUG updating MPS...
2022-07-20 23:10:51 DEBUG sort done, MPS update size is 70152
2022-07-20 23:10:52 DEBUG MPS updated
2022-07-20 23:10:52 INFO processed layer 3, size of next layer 17550, time: 2.17279s (0h0m2s)
2022-07-20 23:10:52 DEBUG memory required: 274.2KB
2022-07-20 23:11:27 DEBUG updating MPS...
2022-07-20 23:11:27 DEBUG sort done, MPS update size is 403098
2022-07-20 23:11:28 DEBUG MPS updated
2022-07-20 23:11:28 INFO processed layer 4, size of next layer 80730, time: 36.0149s (0h0m36s)
2022-07-20 23:11:28 DEBUG memory required: 1.232MB
2022-07-20 23:18:22 DEBUG updating MPS...
2022-07-20 23:18:22 DEBUG sort done, MPS update size is 1772012
2022-07-20 23:18:32 DEBUG MPS updated
2022-07-20 23:18:32 INFO processed layer 5, size of next layer 296010, time: 424.378s (0h7m4s)
2022-07-20 23:18:32 DEBUG memory required: 4.517MB
2022-07-21 00:18:32 DEBUG updating MPS...
2022-07-21 00:18:32 DEBUG sort done, MPS update size is 6194958
2022-07-21 00:23:24 DEBUG MPS updated
2022-07-21 00:23:24 INFO processed layer 6, size of next layer 0, time: 3891.84s (1h4m51s)
2022-07-21 00:23:24 DEBUG memory required: 0B
2022-07-21 00:23:24 INFO writing MPS with 8449722 parent sets
2022-07-21 00:23:32 INFO searching done in 5081.62s (1h24m41s)
