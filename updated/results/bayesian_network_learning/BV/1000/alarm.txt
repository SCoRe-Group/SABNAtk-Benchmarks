2022-07-03 16:29:43 INFO input: my_data/1000/alarm.csv
2022-07-03 16:29:43 INFO variables: 37
2022-07-03 16:29:43 INFO instances: 1000
2022-07-03 16:29:43 INFO function: 
2022-07-03 16:29:43 INFO limit: 6
2022-07-03 16:29:43 INFO priors: 
2022-07-03 16:29:43 INFO output: tmp.mps
2022-07-03 16:29:43 INFO searching parent sets
2022-07-03 16:29:43 DEBUG processing with N = 1
2022-07-03 16:29:43 DEBUG building BVCounter engine
2022-07-03 16:29:43 DEBUG engine ready in 96ms
2022-07-03 16:29:43 DEBUG building MDLEngine...
2022-07-03 16:29:43 DEBUG MDLEngine getting entropies...
2022-07-03 16:29:43 DEBUG variables reordered for efficiency
2022-07-03 16:29:43 INFO maximal number of parents limited to 6
2022-07-03 16:29:43 DEBUG MDLEngine ready!
2022-07-03 16:29:43 DEBUG updating MPS...
2022-07-03 16:29:43 DEBUG sort done, MPS update size is 37
2022-07-03 16:29:43 DEBUG MPS updated
2022-07-03 16:29:43 INFO processed layer 0, size of next layer 37, time: 3.7854e-05s (0h0m0s)
2022-07-03 16:29:43 DEBUG memory required: 592B
2022-07-03 16:29:43 DEBUG updating MPS...
2022-07-03 16:29:43 DEBUG sort done, MPS update size is 352
2022-07-03 16:29:43 DEBUG MPS updated
2022-07-03 16:29:43 INFO processed layer 1, size of next layer 666, time: 0.000684449s (0h0m0s)
2022-07-03 16:29:43 DEBUG memory required: 10.41KB
2022-07-03 16:29:43 DEBUG updating MPS...
2022-07-03 16:29:43 DEBUG sort done, MPS update size is 540
2022-07-03 16:29:43 DEBUG MPS updated
2022-07-03 16:29:43 INFO processed layer 2, size of next layer 7770, time: 0.0238984s (0h0m0s)
2022-07-03 16:29:43 DEBUG memory required: 121.4KB
2022-07-03 16:29:43 DEBUG updating MPS...
2022-07-03 16:29:43 DEBUG sort done, MPS update size is 26
2022-07-03 16:29:43 DEBUG MPS updated
2022-07-03 16:29:43 INFO processed layer 3, size of next layer 66045, time: 0.499155s (0h0m0s)
2022-07-03 16:29:43 DEBUG memory required: 1.008MB
2022-07-03 16:29:50 DEBUG updating MPS...
2022-07-03 16:29:50 DEBUG sort done, MPS update size is 0
2022-07-03 16:29:50 DEBUG MPS updated
2022-07-03 16:29:50 INFO processed layer 4, size of next layer 367423, time: 6.40382s (0h0m6s)
2022-07-03 16:29:50 DEBUG memory required: 5.606MB
2022-07-03 16:30:15 DEBUG updating MPS...
2022-07-03 16:30:15 DEBUG sort done, MPS update size is 0
2022-07-03 16:30:15 DEBUG MPS updated
2022-07-03 16:30:15 INFO processed layer 5, size of next layer 406528, time: 25.0036s (0h0m25s)
2022-07-03 16:30:15 DEBUG memory required: 6.203MB
2022-07-03 16:30:31 DEBUG updating MPS...
2022-07-03 16:30:31 DEBUG sort done, MPS update size is 0
2022-07-03 16:30:31 DEBUG MPS updated
2022-07-03 16:30:31 INFO processed layer 6, size of next layer 0, time: 16.3454s (0h0m16s)
2022-07-03 16:30:31 DEBUG memory required: 0B
2022-07-03 16:30:31 INFO writing MPS with 955 parent sets
2022-07-03 16:30:31 INFO searching done in 48.2875s (0h0m48s)
