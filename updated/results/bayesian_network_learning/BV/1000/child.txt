2022-07-01 14:17:46 INFO input: my_data/1000/child.csv
2022-07-01 14:17:46 INFO variables: 20
2022-07-01 14:17:46 INFO instances: 1000
2022-07-01 14:17:46 INFO function: 
2022-07-01 14:17:46 INFO limit: 6
2022-07-01 14:17:46 INFO priors: 
2022-07-01 14:17:46 INFO output: tmp.mps
2022-07-01 14:17:46 INFO searching parent sets
2022-07-01 14:17:46 DEBUG processing with N = 1
2022-07-01 14:17:46 DEBUG building BVCounter engine
2022-07-01 14:17:46 DEBUG engine ready in 54ms
2022-07-01 14:17:46 DEBUG building MDLEngine...
2022-07-01 14:17:46 DEBUG MDLEngine getting entropies...
2022-07-01 14:17:46 DEBUG variables reordered for efficiency
2022-07-01 14:17:46 INFO maximal number of parents limited to 6
2022-07-01 14:17:46 DEBUG MDLEngine ready!
2022-07-01 14:17:46 DEBUG updating MPS...
2022-07-01 14:17:46 DEBUG sort done, MPS update size is 20
2022-07-01 14:17:46 DEBUG MPS updated
2022-07-01 14:17:46 INFO processed layer 0, size of next layer 20, time: 2.9103e-05s (0h0m0s)
2022-07-01 14:17:46 DEBUG memory required: 320B
2022-07-01 14:17:46 DEBUG updating MPS...
2022-07-01 14:17:46 DEBUG sort done, MPS update size is 164
2022-07-01 14:17:46 DEBUG MPS updated
2022-07-01 14:17:46 INFO processed layer 1, size of next layer 190, time: 0.000216028s (0h0m0s)
2022-07-01 14:17:46 DEBUG memory required: 2.969KB
2022-07-01 14:17:46 DEBUG updating MPS...
2022-07-01 14:17:46 DEBUG sort done, MPS update size is 163
2022-07-01 14:17:46 DEBUG MPS updated
2022-07-01 14:17:46 INFO processed layer 2, size of next layer 1140, time: 0.00409343s (0h0m0s)
2022-07-01 14:17:46 DEBUG memory required: 17.81KB
2022-07-01 14:17:46 DEBUG updating MPS...
2022-07-01 14:17:46 DEBUG sort done, MPS update size is 3
2022-07-01 14:17:46 DEBUG MPS updated
2022-07-01 14:17:46 INFO processed layer 3, size of next layer 4845, time: 0.0545337s (0h0m0s)
2022-07-01 14:17:46 DEBUG memory required: 75.7KB
2022-07-01 14:17:46 DEBUG updating MPS...
2022-07-01 14:17:46 DEBUG sort done, MPS update size is 0
2022-07-01 14:17:46 DEBUG MPS updated
2022-07-01 14:17:46 INFO processed layer 4, size of next layer 12514, time: 0.440401s (0h0m0s)
2022-07-01 14:17:46 DEBUG memory required: 195.5KB
2022-07-01 14:17:47 DEBUG updating MPS...
2022-07-01 14:17:47 DEBUG sort done, MPS update size is 0
2022-07-01 14:17:47 DEBUG MPS updated
2022-07-01 14:17:47 INFO processed layer 5, size of next layer 11172, time: 1.27855s (0h0m1s)
2022-07-01 14:17:47 DEBUG memory required: 174.6KB
2022-07-01 14:17:48 DEBUG updating MPS...
2022-07-01 14:17:48 DEBUG sort done, MPS update size is 0
2022-07-01 14:17:48 DEBUG MPS updated
2022-07-01 14:17:48 INFO processed layer 6, size of next layer 0, time: 0.828877s (0h0m0s)
2022-07-01 14:17:48 DEBUG memory required: 0B
2022-07-01 14:17:48 INFO writing MPS with 350 parent sets
2022-07-01 14:17:48 INFO searching done in 2.61113s (0h0m2s)
