2022-07-03 21:07:07 INFO input: my_data/10000/insurance.csv
2022-07-03 21:07:07 INFO variables: 27
2022-07-03 21:07:07 INFO instances: 10000
2022-07-03 21:07:07 INFO function: 
2022-07-03 21:07:07 INFO limit: 6
2022-07-03 21:07:07 INFO priors: 
2022-07-03 21:07:07 INFO output: tmp.mps
2022-07-03 21:07:07 INFO searching parent sets
2022-07-03 21:07:07 DEBUG processing with N = 1
2022-07-03 21:07:07 DEBUG building BVCounter engine
2022-07-03 21:07:07 DEBUG engine ready in 521ms
2022-07-03 21:07:07 DEBUG building MDLEngine...
2022-07-03 21:07:07 DEBUG MDLEngine getting entropies...
2022-07-03 21:07:07 DEBUG variables reordered for efficiency
2022-07-03 21:07:07 INFO maximal number of parents limited to 6
2022-07-03 21:07:07 DEBUG MDLEngine ready!
2022-07-03 21:07:07 DEBUG updating MPS...
2022-07-03 21:07:07 DEBUG sort done, MPS update size is 27
2022-07-03 21:07:07 DEBUG MPS updated
2022-07-03 21:07:07 INFO processed layer 0, size of next layer 27, time: 3.6949e-05s (0h0m0s)
2022-07-03 21:07:07 DEBUG memory required: 432B
2022-07-03 21:07:07 DEBUG updating MPS...
2022-07-03 21:07:07 DEBUG sort done, MPS update size is 406
2022-07-03 21:07:07 DEBUG MPS updated
2022-07-03 21:07:07 INFO processed layer 1, size of next layer 351, time: 0.00115059s (0h0m0s)
2022-07-03 21:07:07 DEBUG memory required: 5.484KB
2022-07-03 21:07:07 DEBUG updating MPS...
2022-07-03 21:07:07 DEBUG sort done, MPS update size is 1464
2022-07-03 21:07:07 DEBUG MPS updated
2022-07-03 21:07:07 INFO processed layer 2, size of next layer 2925, time: 0.0444983s (0h0m0s)
2022-07-03 21:07:07 DEBUG memory required: 45.7KB
2022-07-03 21:07:08 DEBUG updating MPS...
2022-07-03 21:07:08 DEBUG sort done, MPS update size is 862
2022-07-03 21:07:08 DEBUG MPS updated
2022-07-03 21:07:08 INFO processed layer 3, size of next layer 17550, time: 0.970388s (0h0m0s)
2022-07-03 21:07:08 DEBUG memory required: 274.2KB
2022-07-03 21:07:21 DEBUG updating MPS...
2022-07-03 21:07:21 DEBUG sort done, MPS update size is 85
2022-07-03 21:07:21 DEBUG MPS updated
2022-07-03 21:07:21 INFO processed layer 4, size of next layer 80730, time: 13.2898s (0h0m13s)
2022-07-03 21:07:21 DEBUG memory required: 1.232MB
2022-07-03 21:09:18 DEBUG updating MPS...
2022-07-03 21:09:18 DEBUG sort done, MPS update size is 1
2022-07-03 21:09:18 DEBUG MPS updated
2022-07-03 21:09:18 INFO processed layer 5, size of next layer 283987, time: 116.988s (0h1m56s)
2022-07-03 21:09:18 DEBUG memory required: 4.333MB
2022-07-03 21:16:43 DEBUG updating MPS...
2022-07-03 21:16:43 DEBUG sort done, MPS update size is 0
2022-07-03 21:16:43 DEBUG MPS updated
2022-07-03 21:16:43 INFO processed layer 6, size of next layer 0, time: 444.662s (0h7m24s)
2022-07-03 21:16:43 DEBUG memory required: 0B
2022-07-03 21:16:43 INFO writing MPS with 2845 parent sets
2022-07-03 21:16:43 INFO searching done in 576.223s (0h9m36s)
