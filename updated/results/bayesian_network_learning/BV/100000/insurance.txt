2022-07-05 20:29:19 INFO input: my_data/100000/insurance.csv
2022-07-05 20:29:19 INFO variables: 27
2022-07-05 20:29:19 INFO instances: 100000
2022-07-05 20:29:19 INFO function: 
2022-07-05 20:29:19 INFO limit: 6
2022-07-05 20:29:19 INFO priors: 
2022-07-05 20:29:19 INFO output: tmp.mps
2022-07-05 20:29:19 INFO searching parent sets
2022-07-05 20:29:19 DEBUG processing with N = 1
2022-07-05 20:29:19 DEBUG building BVCounter engine
2022-07-05 20:29:19 DEBUG engine ready in 4741ms
2022-07-05 20:29:19 DEBUG building MDLEngine...
2022-07-05 20:29:19 DEBUG MDLEngine getting entropies...
2022-07-05 20:29:36 DEBUG variables reordered for efficiency
2022-07-05 20:29:36 INFO maximal number of parents limited to 6
2022-07-05 20:29:36 DEBUG MDLEngine ready!
2022-07-05 20:29:36 DEBUG updating MPS...
2022-07-05 20:29:36 DEBUG sort done, MPS update size is 27
2022-07-05 20:29:36 DEBUG MPS updated
2022-07-05 20:29:36 INFO processed layer 0, size of next layer 27, time: 4.0589e-05s (0h0m0s)
2022-07-05 20:29:36 DEBUG memory required: 432B
2022-07-05 20:29:36 DEBUG updating MPS...
2022-07-05 20:29:36 DEBUG sort done, MPS update size is 532
2022-07-05 20:29:36 DEBUG MPS updated
2022-07-05 20:29:36 INFO processed layer 1, size of next layer 351, time: 0.00778108s (0h0m0s)
2022-07-05 20:29:36 DEBUG memory required: 5.484KB
2022-07-05 20:29:36 DEBUG updating MPS...
2022-07-05 20:29:36 DEBUG sort done, MPS update size is 3975
2022-07-05 20:29:36 DEBUG MPS updated
2022-07-05 20:29:36 INFO processed layer 2, size of next layer 2925, time: 0.367131s (0h0m0s)
2022-07-05 20:29:36 DEBUG memory required: 45.7KB
2022-07-05 20:29:46 DEBUG updating MPS...
2022-07-05 20:29:46 DEBUG sort done, MPS update size is 9170
2022-07-05 20:29:46 DEBUG MPS updated
2022-07-05 20:29:46 INFO processed layer 3, size of next layer 17550, time: 9.08286s (0h0m9s)
2022-07-05 20:29:46 DEBUG memory required: 274.2KB
2022-07-05 20:32:10 DEBUG updating MPS...
2022-07-05 20:32:10 DEBUG sort done, MPS update size is 5004
2022-07-05 20:32:10 DEBUG MPS updated
2022-07-05 20:32:10 INFO processed layer 4, size of next layer 80730, time: 144.167s (0h2m24s)
2022-07-05 20:32:10 DEBUG memory required: 1.232MB
2022-07-05 20:57:46 DEBUG updating MPS...
2022-07-05 20:57:46 DEBUG sort done, MPS update size is 669
2022-07-05 20:57:46 DEBUG MPS updated
2022-07-05 20:57:46 INFO processed layer 5, size of next layer 296010, time: 1535.94s (0h25m35s)
2022-07-05 20:57:46 DEBUG memory required: 4.517MB
2022-07-06 00:10:20 DEBUG updating MPS...
2022-07-06 00:10:20 DEBUG sort done, MPS update size is 8
2022-07-06 00:10:20 DEBUG MPS updated
2022-07-06 00:10:20 INFO processed layer 6, size of next layer 0, time: 11554.2s (3h12m34s)
2022-07-06 00:10:20 DEBUG memory required: 0B
2022-07-06 00:10:20 INFO writing MPS with 19385 parent sets
2022-07-06 00:10:20 INFO searching done in 13260.6s (3h41m0s)
