2022-08-03 11:28:26 INFO input: my_data/100000/insurance.csv
2022-08-03 11:28:26 INFO variables: 27
2022-08-03 11:28:26 INFO instances: 100000
2022-08-03 11:28:26 INFO function: 
2022-08-03 11:28:26 INFO limit: 6
2022-08-03 11:28:26 INFO priors: 
2022-08-03 11:28:26 INFO output: tmp.mps
2022-08-03 11:28:26 INFO searching parent sets
2022-08-03 11:28:26 DEBUG processing with N = 1
2022-08-03 11:28:26 DEBUG building AutoCounter engine
2022-08-03 11:28:27 DEBUG engine ready in 898391ms
2022-08-03 11:28:27 DEBUG building MDLEngine...
2022-08-03 11:28:27 DEBUG MDLEngine getting entropies...
2022-08-03 11:28:28 DEBUG variables reordered for efficiency
2022-08-03 11:28:28 INFO maximal number of parents limited to 6
2022-08-03 11:28:28 DEBUG MDLEngine ready!
2022-08-03 11:28:28 DEBUG updating MPS...
2022-08-03 11:28:28 DEBUG sort done, MPS update size is 27
2022-08-03 11:28:28 DEBUG MPS updated
2022-08-03 11:28:28 INFO processed layer 0, size of next layer 27, time: 6.1181e-05s (0h0m0s)
2022-08-03 11:28:28 DEBUG memory required: 432B
2022-08-03 11:28:28 DEBUG updating MPS...
2022-08-03 11:28:28 DEBUG sort done, MPS update size is 532
2022-08-03 11:28:28 DEBUG MPS updated
2022-08-03 11:28:28 INFO processed layer 1, size of next layer 351, time: 0.00868262s (0h0m0s)
2022-08-03 11:28:28 DEBUG memory required: 5.484KB
2022-08-03 11:28:28 DEBUG updating MPS...
2022-08-03 11:28:28 DEBUG sort done, MPS update size is 3975
2022-08-03 11:28:28 DEBUG MPS updated
2022-08-03 11:28:28 INFO processed layer 2, size of next layer 2925, time: 0.379494s (0h0m0s)
2022-08-03 11:28:28 DEBUG memory required: 45.7KB
2022-08-03 11:28:38 DEBUG updating MPS...
2022-08-03 11:28:38 DEBUG sort done, MPS update size is 9170
2022-08-03 11:28:38 DEBUG MPS updated
2022-08-03 11:28:38 INFO processed layer 3, size of next layer 17550, time: 9.12344s (0h0m9s)
2022-08-03 11:28:38 DEBUG memory required: 274.2KB
2022-08-03 11:29:39 DEBUG updating MPS...
2022-08-03 11:29:39 DEBUG sort done, MPS update size is 5004
2022-08-03 11:29:39 DEBUG MPS updated
2022-08-03 11:29:39 INFO processed layer 4, size of next layer 80730, time: 61.606s (0h1m1s)
2022-08-03 11:29:39 DEBUG memory required: 1.232MB
2022-08-03 11:34:40 DEBUG updating MPS...
2022-08-03 11:34:40 DEBUG sort done, MPS update size is 669
2022-08-03 11:34:40 DEBUG MPS updated
2022-08-03 11:34:40 INFO processed layer 5, size of next layer 296010, time: 300.884s (0h5m0s)
2022-08-03 11:34:40 DEBUG memory required: 4.517MB
2022-08-03 11:54:07 DEBUG updating MPS...
2022-08-03 11:54:07 DEBUG sort done, MPS update size is 8
2022-08-03 11:54:07 DEBUG MPS updated
2022-08-03 11:54:07 INFO processed layer 6, size of next layer 0, time: 1167.1s (0h19m27s)
2022-08-03 11:54:07 DEBUG memory required: 0B
2022-08-03 11:54:07 INFO total time in processing queries: 1.53608e+12ns
2022-08-03 11:54:07 INFO writing MPS with 19385 parent sets
2022-08-03 11:54:07 INFO searching done in 1540.73s (0h25m40s)
