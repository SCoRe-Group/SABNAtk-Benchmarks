2022-08-03 11:54:07 INFO input: my_data/100000/mildew.csv
2022-08-03 11:54:07 INFO variables: 35
2022-08-03 11:54:07 INFO instances: 100000
2022-08-03 11:54:07 INFO function: 
2022-08-03 11:54:07 INFO limit: 6
2022-08-03 11:54:07 INFO priors: 
2022-08-03 11:54:07 INFO output: tmp.mps
2022-08-03 11:54:07 INFO searching parent sets
2022-08-03 11:54:07 DEBUG processing with N = 1
2022-08-03 11:54:07 DEBUG building AutoCounter engine
2022-08-03 11:54:11 DEBUG engine ready in 3258770ms
2022-08-03 11:54:11 DEBUG building MDLEngine...
2022-08-03 11:54:11 DEBUG MDLEngine getting entropies...
2022-08-03 11:54:12 DEBUG variables reordered for efficiency
2022-08-03 11:54:12 INFO maximal number of parents limited to 6
2022-08-03 11:54:12 DEBUG MDLEngine ready!
2022-08-03 11:54:12 DEBUG updating MPS...
2022-08-03 11:54:12 DEBUG sort done, MPS update size is 35
2022-08-03 11:54:12 DEBUG MPS updated
2022-08-03 11:54:12 INFO processed layer 0, size of next layer 35, time: 7.9554e-05s (0h0m0s)
2022-08-03 11:54:12 DEBUG memory required: 560B
2022-08-03 11:54:12 DEBUG updating MPS...
2022-08-03 11:54:12 DEBUG sort done, MPS update size is 446
2022-08-03 11:54:12 DEBUG MPS updated
2022-08-03 11:54:12 INFO processed layer 1, size of next layer 595, time: 0.248004s (0h0m0s)
2022-08-03 11:54:12 DEBUG memory required: 9.297KB
2022-08-03 11:54:15 DEBUG updating MPS...
2022-08-03 11:54:15 DEBUG sort done, MPS update size is 800
2022-08-03 11:54:15 DEBUG MPS updated
2022-08-03 11:54:15 INFO processed layer 2, size of next layer 6545, time: 2.60716s (0h0m2s)
2022-08-03 11:54:15 DEBUG memory required: 102.3KB
2022-08-03 11:54:55 DEBUG updating MPS...
2022-08-03 11:54:55 DEBUG sort done, MPS update size is 111
2022-08-03 11:54:55 DEBUG MPS updated
2022-08-03 11:54:55 INFO processed layer 3, size of next layer 44794, time: 39.9678s (0h0m39s)
2022-08-03 11:54:55 DEBUG memory required: 699.9KB
2022-08-03 12:00:16 DEBUG updating MPS...
2022-08-03 12:00:16 DEBUG sort done, MPS update size is 0
2022-08-03 12:00:16 DEBUG MPS updated
2022-08-03 12:00:16 INFO processed layer 4, size of next layer 169002, time: 321.275s (0h5m21s)
2022-08-03 12:00:16 DEBUG memory required: 2.579MB
2022-08-03 12:21:08 DEBUG updating MPS...
2022-08-03 12:21:08 DEBUG sort done, MPS update size is 0
2022-08-03 12:21:08 DEBUG MPS updated
2022-08-03 12:21:08 INFO processed layer 5, size of next layer 303531, time: 1251.6s (0h20m51s)
2022-08-03 12:21:08 DEBUG memory required: 4.632MB
2022-08-03 12:54:49 DEBUG updating MPS...
2022-08-03 12:54:49 DEBUG sort done, MPS update size is 0
2022-08-03 12:54:49 DEBUG MPS updated
2022-08-03 12:54:49 INFO processed layer 6, size of next layer 0, time: 2021.81s (0h33m41s)
2022-08-03 12:54:49 DEBUG memory required: 0B
2022-08-03 12:54:49 INFO total time in processing queries: 3.63583e+12ns
2022-08-03 12:54:49 INFO writing MPS with 1392 parent sets
2022-08-03 12:54:49 INFO searching done in 3642.08s (1h0m42s)
