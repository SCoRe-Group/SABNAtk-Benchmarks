2022-08-03 10:52:53 INFO input: my_data/10000/alarm.csv
2022-08-03 10:52:53 INFO variables: 37
2022-08-03 10:52:53 INFO instances: 10000
2022-08-03 10:52:53 INFO function: 
2022-08-03 10:52:53 INFO limit: 6
2022-08-03 10:52:53 INFO priors: 
2022-08-03 10:52:53 INFO output: tmp.mps
2022-08-03 10:52:53 INFO searching parent sets
2022-08-03 10:52:53 DEBUG processing with N = 1
2022-08-03 10:52:53 DEBUG building AutoCounter engine
2022-08-03 10:52:53 DEBUG engine ready in 136753ms
2022-08-03 10:52:53 DEBUG building MDLEngine...
2022-08-03 10:52:53 DEBUG MDLEngine getting entropies...
2022-08-03 10:52:53 DEBUG variables reordered for efficiency
2022-08-03 10:52:53 INFO maximal number of parents limited to 6
2022-08-03 10:52:53 DEBUG MDLEngine ready!
2022-08-03 10:52:53 DEBUG updating MPS...
2022-08-03 10:52:53 DEBUG sort done, MPS update size is 37
2022-08-03 10:52:53 DEBUG MPS updated
2022-08-03 10:52:53 INFO processed layer 0, size of next layer 37, time: 6.9178e-05s (0h0m0s)
2022-08-03 10:52:53 DEBUG memory required: 592B
2022-08-03 10:52:53 DEBUG updating MPS...
2022-08-03 10:52:53 DEBUG sort done, MPS update size is 454
2022-08-03 10:52:53 DEBUG MPS updated
2022-08-03 10:52:53 INFO processed layer 1, size of next layer 666, time: 0.00294536s (0h0m0s)
2022-08-03 10:52:53 DEBUG memory required: 10.41KB
2022-08-03 10:52:54 DEBUG updating MPS...
2022-08-03 10:52:54 DEBUG sort done, MPS update size is 2167
2022-08-03 10:52:54 DEBUG MPS updated
2022-08-03 10:52:54 INFO processed layer 2, size of next layer 7770, time: 0.109402s (0h0m0s)
2022-08-03 10:52:54 DEBUG memory required: 121.4KB
2022-08-03 10:52:56 DEBUG updating MPS...
2022-08-03 10:52:56 DEBUG sort done, MPS update size is 2385
2022-08-03 10:52:56 DEBUG MPS updated
2022-08-03 10:52:56 INFO processed layer 3, size of next layer 66045, time: 2.81549s (0h0m2s)
2022-08-03 10:52:56 DEBUG memory required: 1.008MB
2022-08-03 10:53:36 DEBUG updating MPS...
2022-08-03 10:53:36 DEBUG sort done, MPS update size is 260
2022-08-03 10:53:36 DEBUG MPS updated
2022-08-03 10:53:36 INFO processed layer 4, size of next layer 435897, time: 39.3268s (0h0m39s)
2022-08-03 10:53:36 DEBUG memory required: 6.651MB
2022-08-03 10:58:19 DEBUG updating MPS...
2022-08-03 10:58:19 DEBUG sort done, MPS update size is 2
2022-08-03 10:58:19 DEBUG MPS updated
2022-08-03 10:58:19 INFO processed layer 5, size of next layer 2324147, time: 283.487s (0h4m43s)
2022-08-03 10:58:19 DEBUG memory required: 35.46MB
2022-08-03 11:19:54 DEBUG updating MPS...
2022-08-03 11:19:54 DEBUG sort done, MPS update size is 0
2022-08-03 11:19:54 DEBUG MPS updated
2022-08-03 11:19:54 INFO processed layer 6, size of next layer 0, time: 1295.08s (0h21m35s)
2022-08-03 11:19:54 DEBUG memory required: 0B
2022-08-03 11:19:54 INFO total time in processing queries: 1.6129e+12ns
2022-08-03 11:19:54 INFO writing MPS with 5305 parent sets
2022-08-03 11:19:54 INFO searching done in 1621.08s (0h27m1s)
