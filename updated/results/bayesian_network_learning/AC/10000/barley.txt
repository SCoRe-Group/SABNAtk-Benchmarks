2022-08-03 11:19:54 INFO input: my_data/10000/barley.csv
2022-08-03 11:19:54 INFO variables: 48
2022-08-03 11:19:54 INFO instances: 10000
2022-08-03 11:19:54 INFO function: 
2022-08-03 11:19:54 INFO limit: 6
2022-08-03 11:19:54 INFO priors: 
2022-08-03 11:19:54 INFO output: tmp.mps
2022-08-03 11:19:54 INFO searching parent sets
2022-08-03 11:19:54 DEBUG processing with N = 1
2022-08-03 11:19:54 DEBUG building AutoCounter engine
2022-08-03 11:19:55 DEBUG engine ready in 260099ms
2022-08-03 11:19:55 DEBUG building MDLEngine...
2022-08-03 11:19:55 DEBUG MDLEngine getting entropies...
2022-08-03 11:19:55 DEBUG variables reordered for efficiency
2022-08-03 11:19:55 INFO maximal number of parents limited to 6
2022-08-03 11:19:55 DEBUG MDLEngine ready!
2022-08-03 11:19:55 DEBUG updating MPS...
2022-08-03 11:19:55 DEBUG sort done, MPS update size is 48
2022-08-03 11:19:55 DEBUG MPS updated
2022-08-03 11:19:55 INFO processed layer 0, size of next layer 48, time: 8.8404e-05s (0h0m0s)
2022-08-03 11:19:55 DEBUG memory required: 768B
2022-08-03 11:19:55 DEBUG updating MPS...
2022-08-03 11:19:55 DEBUG sort done, MPS update size is 548
2022-08-03 11:19:55 DEBUG MPS updated
2022-08-03 11:19:55 INFO processed layer 1, size of next layer 1128, time: 0.0198291s (0h0m0s)
2022-08-03 11:19:55 DEBUG memory required: 17.62KB
2022-08-03 11:19:56 DEBUG updating MPS...
2022-08-03 11:19:56 DEBUG sort done, MPS update size is 290
2022-08-03 11:19:56 DEBUG MPS updated
2022-08-03 11:19:56 INFO processed layer 2, size of next layer 17296, time: 0.906421s (0h0m0s)
2022-08-03 11:19:56 DEBUG memory required: 270.2KB
2022-08-03 11:20:24 DEBUG updating MPS...
2022-08-03 11:20:24 DEBUG sort done, MPS update size is 6
2022-08-03 11:20:24 DEBUG MPS updated
2022-08-03 11:20:24 INFO processed layer 3, size of next layer 178008, time: 28.6447s (0h0m28s)
2022-08-03 11:20:24 DEBUG memory required: 2.716MB
2022-08-03 11:25:21 DEBUG updating MPS...
2022-08-03 11:25:21 DEBUG sort done, MPS update size is 0
2022-08-03 11:25:21 DEBUG MPS updated
2022-08-03 11:25:21 INFO processed layer 4, size of next layer 63471, time: 296.15s (0h4m56s)
2022-08-03 11:25:21 DEBUG memory required: 991.7KB
2022-08-03 11:25:42 DEBUG updating MPS...
2022-08-03 11:25:42 DEBUG sort done, MPS update size is 0
2022-08-03 11:25:42 DEBUG MPS updated
2022-08-03 11:25:42 INFO processed layer 5, size of next layer 2013, time: 21.1633s (0h0m21s)
2022-08-03 11:25:42 DEBUG memory required: 31.45KB
2022-08-03 11:25:42 DEBUG updating MPS...
2022-08-03 11:25:42 DEBUG sort done, MPS update size is 0
2022-08-03 11:25:42 DEBUG MPS updated
2022-08-03 11:25:42 INFO processed layer 6, size of next layer 0, time: 0.24256s (0h0m0s)
2022-08-03 11:25:42 DEBUG memory required: 0B
2022-08-03 11:25:42 INFO total time in processing queries: 3.46702e+11ns
2022-08-03 11:25:42 INFO writing MPS with 892 parent sets
2022-08-03 11:25:42 INFO searching done in 347.631s (0h5m47s)
