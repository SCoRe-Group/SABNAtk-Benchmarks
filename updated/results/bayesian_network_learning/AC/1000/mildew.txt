2022-08-03 10:47:29 INFO input: my_data/1000/mildew.csv
2022-08-03 10:47:29 INFO variables: 35
2022-08-03 10:47:29 INFO instances: 1000
2022-08-03 10:47:29 INFO function: 
2022-08-03 10:47:29 INFO limit: 6
2022-08-03 10:47:29 INFO priors: 
2022-08-03 10:47:29 INFO output: tmp.mps
2022-08-03 10:47:29 INFO searching parent sets
2022-08-03 10:47:29 DEBUG processing with N = 1
2022-08-03 10:47:29 DEBUG building AutoCounter engine
2022-08-03 10:47:29 DEBUG engine ready in 53032ms
2022-08-03 10:47:29 DEBUG building MDLEngine...
2022-08-03 10:47:29 DEBUG MDLEngine getting entropies...
2022-08-03 10:47:29 DEBUG variables reordered for efficiency
2022-08-03 10:47:29 INFO maximal number of parents limited to 6
2022-08-03 10:47:29 DEBUG MDLEngine ready!
2022-08-03 10:47:29 DEBUG updating MPS...
2022-08-03 10:47:29 DEBUG sort done, MPS update size is 35
2022-08-03 10:47:29 DEBUG MPS updated
2022-08-03 10:47:29 INFO processed layer 0, size of next layer 35, time: 0.000108866s (0h0m0s)
2022-08-03 10:47:29 DEBUG memory required: 560B
2022-08-03 10:47:29 DEBUG updating MPS...
2022-08-03 10:47:29 DEBUG sort done, MPS update size is 90
2022-08-03 10:47:29 DEBUG MPS updated
2022-08-03 10:47:29 INFO processed layer 1, size of next layer 595, time: 0.00446187s (0h0m0s)
2022-08-03 10:47:29 DEBUG memory required: 9.297KB
2022-08-03 10:47:30 DEBUG updating MPS...
2022-08-03 10:47:30 DEBUG sort done, MPS update size is 4
2022-08-03 10:47:30 DEBUG MPS updated
2022-08-03 10:47:30 INFO processed layer 2, size of next layer 3656, time: 0.115548s (0h0m0s)
2022-08-03 10:47:30 DEBUG memory required: 57.12KB
2022-08-03 10:47:30 DEBUG updating MPS...
2022-08-03 10:47:30 DEBUG sort done, MPS update size is 0
2022-08-03 10:47:30 DEBUG MPS updated
2022-08-03 10:47:30 INFO processed layer 3, size of next layer 9867, time: 0.62238s (0h0m0s)
2022-08-03 10:47:30 DEBUG memory required: 154.2KB
2022-08-03 10:47:31 DEBUG updating MPS...
2022-08-03 10:47:31 DEBUG sort done, MPS update size is 0
2022-08-03 10:47:31 DEBUG MPS updated
2022-08-03 10:47:31 INFO processed layer 4, size of next layer 0, time: 1.15793s (0h0m1s)
2022-08-03 10:47:31 DEBUG memory required: 0B
2022-08-03 10:47:31 INFO total time in processing queries: 1.88838e+09ns
2022-08-03 10:47:31 INFO writing MPS with 129 parent sets
2022-08-03 10:47:31 INFO searching done in 1.96185s (0h0m1s)
