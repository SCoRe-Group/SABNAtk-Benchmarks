2022-08-03 10:47:31 INFO input: my_data/1000/alarm.csv
2022-08-03 10:47:31 INFO variables: 37
2022-08-03 10:47:31 INFO instances: 1000
2022-08-03 10:47:31 INFO function: 
2022-08-03 10:47:31 INFO limit: 6
2022-08-03 10:47:31 INFO priors: 
2022-08-03 10:47:31 INFO output: tmp.mps
2022-08-03 10:47:31 INFO searching parent sets
2022-08-03 10:47:31 DEBUG processing with N = 1
2022-08-03 10:47:31 DEBUG building AutoCounter engine
2022-08-03 10:47:31 DEBUG engine ready in 58597ms
2022-08-03 10:47:31 DEBUG building MDLEngine...
2022-08-03 10:47:31 DEBUG MDLEngine getting entropies...
2022-08-03 10:47:31 DEBUG variables reordered for efficiency
2022-08-03 10:47:31 INFO maximal number of parents limited to 6
2022-08-03 10:47:31 DEBUG MDLEngine ready!
2022-08-03 10:47:31 DEBUG updating MPS...
2022-08-03 10:47:31 DEBUG sort done, MPS update size is 37
2022-08-03 10:47:31 DEBUG MPS updated
2022-08-03 10:47:31 INFO processed layer 0, size of next layer 37, time: 8.2437e-05s (0h0m0s)
2022-08-03 10:47:31 DEBUG memory required: 592B
2022-08-03 10:47:31 DEBUG updating MPS...
2022-08-03 10:47:31 DEBUG sort done, MPS update size is 352
2022-08-03 10:47:31 DEBUG MPS updated
2022-08-03 10:47:31 INFO processed layer 1, size of next layer 666, time: 0.00179555s (0h0m0s)
2022-08-03 10:47:31 DEBUG memory required: 10.41KB
2022-08-03 10:47:31 DEBUG updating MPS...
2022-08-03 10:47:31 DEBUG sort done, MPS update size is 540
2022-08-03 10:47:31 DEBUG MPS updated
2022-08-03 10:47:31 INFO processed layer 2, size of next layer 7770, time: 0.0423531s (0h0m0s)
2022-08-03 10:47:31 DEBUG memory required: 121.4KB
2022-08-03 10:47:32 DEBUG updating MPS...
2022-08-03 10:47:32 DEBUG sort done, MPS update size is 26
2022-08-03 10:47:32 DEBUG MPS updated
2022-08-03 10:47:32 INFO processed layer 3, size of next layer 66045, time: 0.719235s (0h0m0s)
2022-08-03 10:47:32 DEBUG memory required: 1.008MB
2022-08-03 10:47:39 DEBUG updating MPS...
2022-08-03 10:47:39 DEBUG sort done, MPS update size is 0
2022-08-03 10:47:39 DEBUG MPS updated
2022-08-03 10:47:39 INFO processed layer 4, size of next layer 367423, time: 7.06846s (0h0m7s)
2022-08-03 10:47:39 DEBUG memory required: 5.606MB
2022-08-03 10:48:02 DEBUG updating MPS...
2022-08-03 10:48:02 DEBUG sort done, MPS update size is 0
2022-08-03 10:48:02 DEBUG MPS updated
2022-08-03 10:48:02 INFO processed layer 5, size of next layer 406528, time: 22.9159s (0h0m22s)
2022-08-03 10:48:02 DEBUG memory required: 6.203MB
2022-08-03 10:48:17 DEBUG updating MPS...
2022-08-03 10:48:17 DEBUG sort done, MPS update size is 0
2022-08-03 10:48:17 DEBUG MPS updated
2022-08-03 10:48:17 INFO processed layer 6, size of next layer 0, time: 14.5979s (0h0m14s)
2022-08-03 10:48:17 DEBUG memory required: 0B
2022-08-03 10:48:17 INFO total time in processing queries: 4.45497e+10ns
2022-08-03 10:48:17 INFO writing MPS with 955 parent sets
2022-08-03 10:48:17 INFO searching done in 45.4156s (0h0m45s)
