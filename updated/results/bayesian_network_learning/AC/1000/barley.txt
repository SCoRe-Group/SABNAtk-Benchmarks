2022-08-03 10:48:17 INFO input: my_data/1000/barley.csv
2022-08-03 10:48:17 INFO variables: 48
2022-08-03 10:48:17 INFO instances: 1000
2022-08-03 10:48:17 INFO function: 
2022-08-03 10:48:17 INFO limit: 6
2022-08-03 10:48:17 INFO priors: 
2022-08-03 10:48:17 INFO output: tmp.mps
2022-08-03 10:48:17 INFO searching parent sets
2022-08-03 10:48:17 DEBUG processing with N = 1
2022-08-03 10:48:17 DEBUG building AutoCounter engine
2022-08-03 10:48:17 DEBUG engine ready in 54374ms
2022-08-03 10:48:17 DEBUG building MDLEngine...
2022-08-03 10:48:17 DEBUG MDLEngine getting entropies...
2022-08-03 10:48:17 DEBUG variables reordered for efficiency
2022-08-03 10:48:17 INFO maximal number of parents limited to 6
2022-08-03 10:48:17 DEBUG MDLEngine ready!
2022-08-03 10:48:17 DEBUG updating MPS...
2022-08-03 10:48:17 DEBUG sort done, MPS update size is 48
2022-08-03 10:48:17 DEBUG MPS updated
2022-08-03 10:48:17 INFO processed layer 0, size of next layer 48, time: 0.000137807s (0h0m0s)
2022-08-03 10:48:17 DEBUG memory required: 768B
2022-08-03 10:48:17 DEBUG updating MPS...
2022-08-03 10:48:17 DEBUG sort done, MPS update size is 198
2022-08-03 10:48:17 DEBUG MPS updated
2022-08-03 10:48:17 INFO processed layer 1, size of next layer 1128, time: 0.00618311s (0h0m0s)
2022-08-03 10:48:17 DEBUG memory required: 17.62KB
2022-08-03 10:48:17 DEBUG updating MPS...
2022-08-03 10:48:17 DEBUG sort done, MPS update size is 7
2022-08-03 10:48:17 DEBUG MPS updated
2022-08-03 10:48:17 INFO processed layer 2, size of next layer 16216, time: 0.303428s (0h0m0s)
2022-08-03 10:48:17 DEBUG memory required: 253.4KB
2022-08-03 10:48:24 DEBUG updating MPS...
2022-08-03 10:48:24 DEBUG sort done, MPS update size is 0
2022-08-03 10:48:24 DEBUG MPS updated
2022-08-03 10:48:24 INFO processed layer 3, size of next layer 7340, time: 6.4017s (0h0m6s)
2022-08-03 10:48:24 DEBUG memory required: 114.7KB
2022-08-03 10:48:24 DEBUG updating MPS...
2022-08-03 10:48:24 DEBUG sort done, MPS update size is 0
2022-08-03 10:48:24 DEBUG MPS updated
2022-08-03 10:48:24 INFO processed layer 4, size of next layer 201, time: 0.403786s (0h0m0s)
2022-08-03 10:48:24 DEBUG memory required: 3.141KB
2022-08-03 10:48:24 DEBUG updating MPS...
2022-08-03 10:48:24 DEBUG sort done, MPS update size is 0
2022-08-03 10:48:24 DEBUG MPS updated
2022-08-03 10:48:24 INFO processed layer 5, size of next layer 0, time: 0.00209325s (0h0m0s)
2022-08-03 10:48:24 DEBUG memory required: 0B
2022-08-03 10:48:24 INFO total time in processing queries: 7.08549e+09ns
2022-08-03 10:48:24 INFO writing MPS with 253 parent sets
2022-08-03 10:48:24 INFO searching done in 7.18304s (0h0m7s)
