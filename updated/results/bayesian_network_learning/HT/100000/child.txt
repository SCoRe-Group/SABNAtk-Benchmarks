2022-07-01 21:22:42 INFO input: my_data/100000/child.csv
2022-07-01 21:22:42 INFO variables: 20
2022-07-01 21:22:42 INFO instances: 100000
2022-07-01 21:22:42 INFO function: 
2022-07-01 21:22:42 INFO limit: 6
2022-07-01 21:22:42 INFO priors: 
2022-07-01 21:22:42 INFO output: tmp.mps
2022-07-01 21:22:42 INFO searching parent sets
2022-07-01 21:22:42 DEBUG processing with N = 1
2022-07-01 21:22:42 DEBUG building Hasher engine
2022-07-01 21:22:42 DEBUG engine ready in 9941ms
2022-07-01 21:22:42 DEBUG building MDLEngine...
2022-07-01 21:22:42 DEBUG MDLEngine getting entropies...
2022-07-01 21:22:43 INFO maximal number of parents limited to 6
2022-07-01 21:22:43 DEBUG MDLEngine ready!
2022-07-01 21:22:43 DEBUG updating MPS...
2022-07-01 21:22:43 DEBUG sort done, MPS update size is 20
2022-07-01 21:22:43 DEBUG MPS updated
2022-07-01 21:22:43 INFO processed layer 0, size of next layer 20, time: 3.7131e-05s (0h0m0s)
2022-07-01 21:22:43 DEBUG memory required: 320B
2022-07-01 21:22:44 DEBUG updating MPS...
2022-07-01 21:22:44 DEBUG sort done, MPS update size is 380
2022-07-01 21:22:44 DEBUG MPS updated
2022-07-01 21:22:44 INFO processed layer 1, size of next layer 190, time: 1.51523s (0h0m1s)
2022-07-01 21:22:44 DEBUG memory required: 2.969KB
2022-07-01 21:23:06 DEBUG updating MPS...
2022-07-01 21:23:06 DEBUG sort done, MPS update size is 3420
2022-07-01 21:23:06 DEBUG MPS updated
2022-07-01 21:23:06 INFO processed layer 2, size of next layer 1140, time: 21.2562s (0h0m21s)
2022-07-01 21:23:06 DEBUG memory required: 17.81KB
2022-07-01 21:25:44 DEBUG updating MPS...
2022-07-01 21:25:44 DEBUG sort done, MPS update size is 19380
2022-07-01 21:25:44 DEBUG MPS updated
2022-07-01 21:25:44 INFO processed layer 3, size of next layer 4845, time: 158.574s (0h2m38s)
2022-07-01 21:25:44 DEBUG memory required: 75.7KB
2022-07-01 21:39:07 DEBUG updating MPS...
2022-07-01 21:39:07 DEBUG sort done, MPS update size is 77520
2022-07-01 21:39:07 DEBUG MPS updated
2022-07-01 21:39:07 INFO processed layer 4, size of next layer 15504, time: 802.742s (0h13m22s)
2022-07-01 21:39:07 DEBUG memory required: 242.2KB
2022-07-01 22:25:21 DEBUG updating MPS...
2022-07-01 22:25:21 DEBUG sort done, MPS update size is 232560
2022-07-01 22:25:21 DEBUG MPS updated
2022-07-01 22:25:21 INFO processed layer 5, size of next layer 38760, time: 2773.85s (0h46m13s)
2022-07-01 22:25:21 DEBUG memory required: 605.6KB
2022-07-02 00:30:11 DEBUG updating MPS...
2022-07-02 00:30:11 DEBUG sort done, MPS update size is 542640
2022-07-02 00:30:12 DEBUG MPS updated
2022-07-02 00:30:12 INFO processed layer 6, size of next layer 0, time: 7491.47s (2h4m51s)
2022-07-02 00:30:12 DEBUG memory required: 0B
2022-07-02 00:30:12 INFO writing MPS with 875920 parent sets
2022-07-02 00:30:13 INFO searching done in 11251.4s (3h7m31s)
