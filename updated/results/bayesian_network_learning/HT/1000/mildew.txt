2022-07-07 13:06:02 INFO input: my_data/1000/mildew.csv
2022-07-07 13:06:02 INFO variables: 35
2022-07-07 13:06:02 INFO instances: 1000
2022-07-07 13:06:02 INFO function: 
2022-07-07 13:06:02 INFO limit: 6
2022-07-07 13:06:02 INFO priors: 
2022-07-07 13:06:02 INFO output: tmp.mps
2022-07-07 13:06:02 INFO searching parent sets
2022-07-07 13:06:02 DEBUG processing with N = 1
2022-07-07 13:06:02 DEBUG building Hasher engine
2022-07-07 13:06:02 DEBUG engine ready in 221ms
2022-07-07 13:06:02 DEBUG building MDLEngine...
2022-07-07 13:06:02 DEBUG MDLEngine getting entropies...
2022-07-07 13:06:02 INFO maximal number of parents limited to 6
2022-07-07 13:06:02 DEBUG MDLEngine ready!
2022-07-07 13:06:02 DEBUG updating MPS...
2022-07-07 13:06:02 DEBUG sort done, MPS update size is 35
2022-07-07 13:06:02 DEBUG MPS updated
2022-07-07 13:06:02 INFO processed layer 0, size of next layer 35, time: 5.9453e-05s (0h0m0s)
2022-07-07 13:06:02 DEBUG memory required: 560B
2022-07-07 13:06:02 DEBUG updating MPS...
2022-07-07 13:06:02 DEBUG sort done, MPS update size is 1190
2022-07-07 13:06:02 DEBUG MPS updated
2022-07-07 13:06:02 INFO processed layer 1, size of next layer 595, time: 0.0578725s (0h0m0s)
2022-07-07 13:06:02 DEBUG memory required: 9.297KB
2022-07-07 13:06:04 DEBUG updating MPS...
2022-07-07 13:06:04 DEBUG sort done, MPS update size is 19635
2022-07-07 13:06:04 DEBUG MPS updated
2022-07-07 13:06:04 INFO processed layer 2, size of next layer 6545, time: 1.7329s (0h0m1s)
2022-07-07 13:06:04 DEBUG memory required: 102.3KB
2022-07-07 13:06:34 DEBUG updating MPS...
2022-07-07 13:06:34 DEBUG sort done, MPS update size is 209440
2022-07-07 13:06:34 DEBUG MPS updated
2022-07-07 13:06:34 INFO processed layer 3, size of next layer 52360, time: 30.5172s (0h0m30s)
2022-07-07 13:06:34 DEBUG memory required: 818.1KB
2022-07-07 13:12:29 DEBUG updating MPS...
2022-07-07 13:12:29 DEBUG sort done, MPS update size is 1623101
2022-07-07 13:12:33 DEBUG MPS updated
2022-07-07 13:12:33 INFO processed layer 4, size of next layer 324632, time: 358.943s (0h5m58s)
2022-07-07 13:12:33 DEBUG memory required: 4.953MB
2022-07-07 13:59:46 DEBUG updating MPS...
2022-07-07 13:59:46 DEBUG sort done, MPS update size is 9720022
2022-07-07 14:06:32 DEBUG MPS updated
2022-07-07 14:06:32 INFO processed layer 5, size of next layer 1623160, time: 3239.02s (0h53m59s)
2022-07-07 14:06:32 DEBUG memory required: 24.77MB
2022-07-07 20:23:26 DEBUG updating MPS...
2022-07-07 20:23:28 DEBUG sort done, MPS update size is 46160293
2022-07-07 23:39:34 DEBUG MPS updated
2022-07-07 23:39:34 INFO processed layer 6, size of next layer 0, time: 34381.9s (9h33m1s)
2022-07-07 23:39:34 DEBUG memory required: 0B
2022-07-07 23:39:34 INFO writing MPS with 57733716 parent sets
2022-07-07 23:40:21 INFO searching done in 38058.9s (10h34m18s)
