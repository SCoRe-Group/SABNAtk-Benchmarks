2022-07-07 12:10:21 INFO input: my_data/1000/insurance.csv
2022-07-07 12:10:21 INFO variables: 27
2022-07-07 12:10:21 INFO instances: 1000
2022-07-07 12:10:21 INFO function: 
2022-07-07 12:10:21 INFO limit: 6
2022-07-07 12:10:21 INFO priors: 
2022-07-07 12:10:21 INFO output: tmp.mps
2022-07-07 12:10:21 INFO searching parent sets
2022-07-07 12:10:21 DEBUG processing with N = 1
2022-07-07 12:10:21 DEBUG building Hasher engine
2022-07-07 12:10:21 DEBUG engine ready in 1148ms
2022-07-07 12:10:21 DEBUG building MDLEngine...
2022-07-07 12:10:21 DEBUG MDLEngine getting entropies...
2022-07-07 12:10:21 INFO maximal number of parents limited to 6
2022-07-07 12:10:21 DEBUG MDLEngine ready!
2022-07-07 12:10:21 DEBUG updating MPS...
2022-07-07 12:10:21 DEBUG sort done, MPS update size is 27
2022-07-07 12:10:21 DEBUG MPS updated
2022-07-07 12:10:21 INFO processed layer 0, size of next layer 27, time: 5.2135e-05s (0h0m0s)
2022-07-07 12:10:21 DEBUG memory required: 432B
2022-07-07 12:10:21 DEBUG updating MPS...
2022-07-07 12:10:21 DEBUG sort done, MPS update size is 702
2022-07-07 12:10:21 DEBUG MPS updated
2022-07-07 12:10:21 INFO processed layer 1, size of next layer 351, time: 0.029539s (0h0m0s)
2022-07-07 12:10:21 DEBUG memory required: 5.484KB
2022-07-07 12:10:22 DEBUG updating MPS...
2022-07-07 12:10:22 DEBUG sort done, MPS update size is 8759
2022-07-07 12:10:22 DEBUG MPS updated
2022-07-07 12:10:22 INFO processed layer 2, size of next layer 2925, time: 0.574092s (0h0m0s)
2022-07-07 12:10:22 DEBUG memory required: 45.7KB
2022-07-07 12:10:28 DEBUG updating MPS...
2022-07-07 12:10:28 DEBUG sort done, MPS update size is 69773
2022-07-07 12:10:28 DEBUG MPS updated
2022-07-07 12:10:28 INFO processed layer 3, size of next layer 17550, time: 6.02327s (0h0m6s)
2022-07-07 12:10:28 DEBUG memory required: 274.2KB
2022-07-07 12:11:14 DEBUG updating MPS...
2022-07-07 12:11:14 DEBUG sort done, MPS update size is 398936
2022-07-07 12:11:14 DEBUG MPS updated
2022-07-07 12:11:14 INFO processed layer 4, size of next layer 80730, time: 46.0213s (0h0m46s)
2022-07-07 12:11:14 DEBUG memory required: 1.232MB
2022-07-07 12:15:21 DEBUG updating MPS...
2022-07-07 12:15:21 DEBUG sort done, MPS update size is 1739329
2022-07-07 12:15:31 DEBUG MPS updated
2022-07-07 12:15:31 INFO processed layer 5, size of next layer 296010, time: 256.804s (0h4m16s)
2022-07-07 12:15:31 DEBUG memory required: 4.517MB
2022-07-07 12:34:55 DEBUG updating MPS...
2022-07-07 12:34:55 DEBUG sort done, MPS update size is 5999013
2022-07-07 12:39:04 DEBUG MPS updated
2022-07-07 12:39:04 INFO processed layer 6, size of next layer 0, time: 1413.47s (0h23m33s)
2022-07-07 12:39:04 DEBUG memory required: 0B
2022-07-07 12:39:04 INFO writing MPS with 8216539 parent sets
2022-07-07 12:39:13 INFO searching done in 1731.4s (0h28m51s)
