2022-07-01 18:55:43 INFO input: my_data/1000/child.csv
2022-07-01 18:55:43 INFO variables: 20
2022-07-01 18:55:43 INFO instances: 1000
2022-07-01 18:55:43 INFO function: 
2022-07-01 18:55:43 INFO limit: 6
2022-07-01 18:55:43 INFO priors: 
2022-07-01 18:55:43 INFO output: tmp.mps
2022-07-01 18:55:43 INFO searching parent sets
2022-07-01 18:55:43 DEBUG processing with N = 1
2022-07-01 18:55:43 DEBUG building Hasher engine
2022-07-01 18:55:43 DEBUG engine ready in 103ms
2022-07-01 18:55:43 DEBUG building MDLEngine...
2022-07-01 18:55:43 DEBUG MDLEngine getting entropies...
2022-07-01 18:55:43 INFO maximal number of parents limited to 6
2022-07-01 18:55:43 DEBUG MDLEngine ready!
2022-07-01 18:55:43 DEBUG updating MPS...
2022-07-01 18:55:43 DEBUG sort done, MPS update size is 20
2022-07-01 18:55:43 DEBUG MPS updated
2022-07-01 18:55:43 INFO processed layer 0, size of next layer 20, time: 3.6473e-05s (0h0m0s)
2022-07-01 18:55:43 DEBUG memory required: 320B
2022-07-01 18:55:43 DEBUG updating MPS...
2022-07-01 18:55:43 DEBUG sort done, MPS update size is 380
2022-07-01 18:55:43 DEBUG MPS updated
2022-07-01 18:55:43 INFO processed layer 1, size of next layer 190, time: 0.0158864s (0h0m0s)
2022-07-01 18:55:43 DEBUG memory required: 2.969KB
2022-07-01 18:55:43 DEBUG updating MPS...
2022-07-01 18:55:43 DEBUG sort done, MPS update size is 3420
2022-07-01 18:55:43 DEBUG MPS updated
2022-07-01 18:55:43 INFO processed layer 2, size of next layer 1140, time: 0.219799s (0h0m0s)
2022-07-01 18:55:43 DEBUG memory required: 17.81KB
2022-07-01 18:55:45 DEBUG updating MPS...
2022-07-01 18:55:45 DEBUG sort done, MPS update size is 19380
2022-07-01 18:55:45 DEBUG MPS updated
2022-07-01 18:55:45 INFO processed layer 3, size of next layer 4845, time: 1.68089s (0h0m1s)
2022-07-01 18:55:45 DEBUG memory required: 75.7KB
2022-07-01 18:55:54 DEBUG updating MPS...
2022-07-01 18:55:54 DEBUG sort done, MPS update size is 77520
2022-07-01 18:55:54 DEBUG MPS updated
2022-07-01 18:55:54 INFO processed layer 4, size of next layer 15504, time: 8.86147s (0h0m8s)
2022-07-01 18:55:54 DEBUG memory required: 242.2KB
2022-07-01 18:56:26 DEBUG updating MPS...
2022-07-01 18:56:26 DEBUG sort done, MPS update size is 232560
2022-07-01 18:56:27 DEBUG MPS updated
2022-07-01 18:56:27 INFO processed layer 5, size of next layer 38760, time: 32.8721s (0h0m32s)
2022-07-01 18:56:27 DEBUG memory required: 605.6KB
2022-07-01 18:58:02 DEBUG updating MPS...
2022-07-01 18:58:02 DEBUG sort done, MPS update size is 542640
2022-07-01 18:58:04 DEBUG MPS updated
2022-07-01 18:58:04 INFO processed layer 6, size of next layer 0, time: 96.9406s (0h1m36s)
2022-07-01 18:58:04 DEBUG memory required: 0B
2022-07-01 18:58:04 INFO writing MPS with 875920 parent sets
2022-07-01 18:58:04 INFO searching done in 141.361s (0h2m21s)
