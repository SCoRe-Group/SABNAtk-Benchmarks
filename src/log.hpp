/***
 *  $Id$
 **
 *  File: log.hpp
 *  Created: May 11, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef LOG_HPP
#define LOG_HPP

#include <cstdlib>
#include <jaz/logger.hpp>


extern jaz::Logger Log;


inline void InitLog() {
    if (const char* env = std::getenv("SABNA_LOGGER")) {
        if (std::strncmp(env, "DEBUG", 5) == 0) Log.level(jaz::Logger::DEBUG);
        else if (std::strncmp(env, "INFO", 4) == 0) Log.level(jaz::Logger::INFO);
        else if (std::strncmp(env, "WARN", 4) == 0) Log.level(jaz::Logger::WARN);
        else if (std::strncmp(env, "ERROR", 5) == 0) Log.level(jaz::Logger::ERROR);
    }
} // InitLog()

#endif // LOG_HPP
