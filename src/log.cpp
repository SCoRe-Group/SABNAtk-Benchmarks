/***
 *  $Id$
 **
 *  File: log.cpp
 *  Created: May 11, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <jaz/logger.hpp>

jaz::Logger Log(std::cout);
