#include <chrono>
#include <iostream>
#include <vector>

#include <bit_util.hpp>
#include <sabnatk.hpp>

#include <jaz/timer.hpp>
#include <tbb/task_group.h>

#include "circular_buffer.hpp"
#include "csv.hpp"
#include "read_queries.hpp"


class estimator {
public:
    void init(int ri, int qi) { state_ = 0.0; }

    void finalize(int qi) { }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) { state_ = static_cast<double>(Nijk) / Nij; }

    double state() const { return state_; }

private:
    double state_ = 0.0;

}; // class estimator


const int N = 1;

using set_type = uint_type<N>;
using data_type = uint8_t;

circular_block_buffer<std::pair<set_type, set_type>, 64> CB;

template <typename Engine>
inline void run_query(int pos, Engine& eng) {
    std::vector<estimator> F(1);

    auto acc = CB.hard_get(pos);
    auto& queries = *acc;

    for (const auto& q : queries) eng.apply(q.first, q.second, F);
} // run_query


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "usage: random_query_omp csv_file dat_file" << std::endl;
        return -1;
    }

    // GET INPUT
    std::cout << "reading input..." << std::endl;

    std::vector<data_type> D;

    auto [res, n, m] = read_csv(argv[1], D);

    if (res == false) {
        std::cout << "error: unable to read input data" << std::endl;
        return -1;
    }

    std::cerr << "n=" << n << ", m=" << m << std::endl;


    std::vector<std::pair<set_type, set_type>> Q;

    if (!read_queries(argv[2], Q)) {
        std::cout << "error: unable to read queries" << std::endl;
        return -1;
    }

    int qs = Q.size();
    std::cerr << "qs=" << qs << std::endl;


    // CREATE ENGINE
    std::cout << "building engine..." << std::endl;

    auto eng = sabnatk::create_counter<N, sabnatk::Rad>(n, m, std::begin(D));


    // RUN TEST
    std::cout << "running queries..." << std::endl;

    jaz::timer T;
    tbb::task_group tg;

    CB.set_block_size(16);
    std::pair<int, int> pos;

    for (int i = 0; i < qs; ++i) {
        pos = CB.put(Q[i]);
        if (pos.first != -1) tg.run([pos, &eng]{ run_query(pos.first, eng); });
    }

    pos = CB.put();
    if (pos.first != -1) tg.run([pos, &eng]{ run_query(pos.first, eng); });

    tg.wait();
    std::cout << T.elapsed() << std::endl;

    return 0;
} // main
