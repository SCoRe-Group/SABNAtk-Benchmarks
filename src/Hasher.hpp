/***
 *    $Id$
 **
 *    File: Hasher.hpp
 *    Created: Feb 12, 2018
 *
 *    Authors: Subhadeep Karan <skaran@buffalo.edu>
 *    Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *    Distributed under the MIT License.
 *    See accompanying file LICENSE.
 */


#ifndef HASHER_HPP
#define HASHER_HPP

#include <iostream>
#include <unordered_map>
#include <vector>

template<int N, typename data_type = uint8_t> class Hasher {
public:
    using set_type = uint_type<N>;
    using pair_data_type = std::pair<data_type, data_type>;

    int n() const { return n_; }

    int m() const { return m_; }

    int r(int xi) const { return r_[xi]; }

    bool is_reorderable() { return false; }

    bool reorder(const std::vector<int>& norder) { return false; }


    template<typename score_functor>
    void apply(const set_type& set_xi, const set_type& pa, const std::vector<data_type>& state_xi, const std::vector<data_type>& state_pa, std::vector<score_functor>& F) {
        std::vector<pair_data_type> state_range_pa(state_pa.size());
        std::vector<pair_data_type> state_range_xi(state_xi.size());

        int max_rxi_size = 0;

        for (int xi = 0, idx_state_xi = 0, idx_state_pa = 0; xi < n_; ++xi) {
            if (in_set(pa, xi)) {
                data_type state = state_pa[idx_state_pa];
                state_range_pa[idx_state_pa++] = {state, state + 1};
            }
            else if (in_set(set_xi, xi)) {
                data_type state = state_xi[idx_state_xi];
                state_range_xi[idx_state_xi++] = {state, state + 1};
            }
        }

        m_apply__(set_xi, pa, state_range_xi, state_range_pa, F);
    } // apply (state_specific_queries)

    template<typename score_functor>
    void apply(const set_type& set_xi, const set_type& pa, std::vector<score_functor>& F) {
        std::vector<pair_data_type> state_range_pa(set_size(pa));
        std::vector<pair_data_type> state_range_xi(F.size());

        for (int xi = 0, idx_pa = 0, idx_xi = 0; xi < n_; ++xi) {
            if (in_set(pa, xi)) { state_range_pa[idx_pa++] = {0, r_[xi]}; }
            else if (in_set(set_xi, xi)) { state_range_xi[idx_xi++] = {0, r_[xi] }; }
        }
    
        m_apply__(set_xi, pa, state_range_xi, state_range_pa, F);
    } // apply 
    

private:   
    template<int M, typename data_type_copy, typename Iter_copy>
    friend Hasher<M, data_type_copy> create_Hasher(const int n, const int m, Iter_copy it);


    template<typename score_functor>
    void m_apply__(const set_type& set_xi, const set_type& pa, const std::vector<pair_data_type>& state_range_xi, const std::vector<pair_data_type>& state_range_pa, score_functor& F) {
        int q_pa = m_compute_q_pa__(pa, state_range_pa);
        std::vector<int> vec_xi = as_vector(set_xi);
        std::vector<int> vec_pa = as_vector(pa);
        int pa_size = vec_pa.size();
        int xi_size = vec_xi.size();
       
        for (int i = 0; i < xi_size; ++i) {
            const int xi = vec_xi[i];
            F[i].init(r_[xi], q_pa);
        }
 
        if (pa_size == 0) {
            for (int i = 0; i < xi_size; ++i) {
                const int xi = vec_xi[i];
                F[i](m_);
                for (int r = state_range_xi[i].first; r < state_range_xi[i].second; ++r) { F[i](xi_r_count_[xi][r], m_); }
            }
            return;
        }        

        int sum_r_xi = 0;
        std::vector<int> r_idx;
        for (const int xi : vec_xi) { 
            r_idx.push_back(sum_r_xi);
            sum_r_xi += r_[xi]; 
        }
        
        std::unordered_map<std::string, std::vector<int>> ht;
       
        std::string key; 
        for (int rid = 0; rid < m_; ++rid) {
            key = "";
            bool skip_current_row = false;
            for (int i = 0; i < pa_size && !skip_current_row; ++i) {
                const int xi = vec_pa[i];
                const data_type temp = D_[rid * n_ + xi];
                if (temp >= state_range_pa[i].first && temp < state_range_pa[i].second) { key += " " + std::to_string(temp); }
                else { skip_current_row = true; }
            }
    
            if (skip_current_row) { continue; }

            auto it_ht = ht.find(key);
            if (it_ht == ht.end()) {
                it_ht = ht.insert({key, std::vector<int>(sum_r_xi + 1, 0)}).first;
            }
            ++(it_ht->second.back());
                
           for (int i = 0; i < xi_size; ++i) {
                const int xi = vec_xi[i];
                const data_type temp = D_[rid * n_ + xi];
                if (temp >= state_range_xi[i].first && temp < state_range_xi[i].second) { ++(it_ht->second[r_idx[i] + temp]); }
           }
        }
    
        for (const auto kv : ht) {
            const int Nij = kv.second.back();
            if (Nij <= 0) { continue; }
            for (int i = 0; i < xi_size; ++i) {
                F[i](Nij);
                for (int r = state_range_xi[i].first; r < state_range_xi[i].second; ++r) {
                    const int Nijk = kv.second[r_idx[i] + r];
                    if (Nijk != 0) { F[i](Nijk, Nij); }
                }
            }
        }
    
    } // m_apply__

    int m_compute_q_pa__(const set_type& pa, const std::vector<pair_data_type>& state_range_pa) const {
        int q = 1;

        for (int xi = 0, idx_xi = 0; xi < n_; ++xi) {
            if (in_set(pa, xi)) { 
                q *= state_range_pa[idx_xi].second - state_range_pa[idx_xi].first; 
                ++idx_xi;
            }
        }

        return q;
    } //m_compute_q_pa__
 
    int n_ = -1;
    int m_ = -1;
    std::vector<data_type> D_;
    std::vector<int> r_;
    std::vector<std::vector<int>> xi_r_count_;    

}; // class Hasher

template<int N, typename data_type, typename Iter>
Hasher<N, data_type> create_Hasher(int n, int m, Iter it) {
    Hasher<N, data_type> cqe;

    cqe.n_ = n;
    cqe.m_ = m;
    cqe.D_.resize(n * m);    
    cqe.r_.resize(n, -1);

    std::vector<data_type> tempD;
    tempD.reserve(n*m);
    cqe.xi_r_count_.resize(n, std::vector<int>());
    
    for (int i = 0; i < n * m; ++i, ++it) { tempD.push_back(*it); }

    for (int xi = 0; xi < n; ++xi) {
        auto min_max = std::minmax_element(tempD.begin() + xi * m, tempD.begin() + (xi+1) * m);
        cqe.r_[xi] = *min_max.second - *min_max.first + 1;
        cqe.xi_r_count_[xi].resize(cqe.r_[xi], 0);
        for (int i = 0; i < m; ++i) { 
            data_type state = tempD[xi * m + i] - *min_max.first;
            cqe.D_[i * n + xi] = state; 
            ++cqe.xi_r_count_[xi][state];
        }
    }
 
    return cqe;
} // create_hasher

#endif
