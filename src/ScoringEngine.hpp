/***
 *  $Id$
 **
 *  File: ScoreEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef SCORING_ENGINE_HPP
#define SCORING_ENGINE_HPP

#include <vector>

#include "MPS.hpp"
#include "MPSList.hpp"

#include <bit_util.hpp>


template <int N>
class ScoringEngine {
public:
    using set_type = uint_type<N>;

    ScoringEngine(int ps = -1) : pa_size(ps) { }

    virtual ~ScoringEngine() { }

    virtual int n() const = 0;

    virtual int m() const = 0;

    virtual bool process(const set_type& pa, set_type& ch, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer, int l) = 0;

    virtual void finalize(MPSList<N>& mps_list) = 0;

protected:
    int pa_size = -1;

}; // class ScoringEngine

#endif // SCORING_ENGINE_HPP
