#include <chrono>
#include <iostream>
#include <vector>

#include <papi.h>

#include <bit_util.hpp>
#include <sabnatk.hpp>

#include "ADTree.hpp"
#include "csv.hpp"
#include "read_queries.hpp"


class estimator {
public:
    void init(int ri, int qi) { state_ = 0.0; }

    void finalize(int qi) { }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) { state_ = static_cast<double>(Nijk) / Nij; }

    double state() const { return state_; }

private:
    double state_ = 0.0;

}; // class estimator


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "usage: random_query csv_file dat_file" << std::endl;
        return -1;
    }

    // GET INPUT
    std::cout << "reading input..." << std::endl;

    const int N = 1;

    using set_type = uint_type<N>;
    using data_type = uint8_t;

    std::vector<data_type> D;

    auto [res, n, m] = read_csv(argv[1], D);

    if (res == false) {
        std::cout << "error: unable to read input data" << std::endl;
        return -1;
    }

    std::cerr << "n=" << n << ", m=" << m << std::endl;


    std::vector<std::pair<set_type, set_type>> Q;

    if (!read_queries(argv[2], Q)) {
        std::cout << "error: unable to read queries" << std::endl;
        return -1;
    }

    std::cerr << "qs=" << Q.size() << std::endl;


    // CREATE ENGINE
    std::cout << "building engine..." << std::endl;

    //ADTree<N, data_type> eng(16, n, m, D);
    //eng.build_tree();

    auto eng = sabnatk::create_counter<N, sabnatk::Rad>(n, m, std::begin(D));


    // RUN TEST
    std::cout << "running queries..." << std::endl;

    const int R = 1;


    std::vector<estimator> F(1);


    int rv = PAPI_library_init(PAPI_VER_CURRENT);

    if ((rv != PAPI_VER_CURRENT) && (rv > 0)) {
        std::cerr << "PAPI failed to initialize: " << rv << std::endl;
        return -1;
    }

    if (rv < 0) {
        std::cerr << "PAPI init error: " << rv << std::endl;
        return -1;
    }

    int eventset = PAPI_NULL;
    rv = PAPI_create_eventset(&eventset);

    if (rv != PAPI_OK) {
        std::cerr << "PAPI failed to create eventset" << std::endl;
        return -1;
    }

    PAPI_add_named_event(eventset,"PAPI_TOT_CYC");
    PAPI_start(eventset);

    long long int count = 0;


    for (const auto& q : Q) {
        // auto t0 = std::chrono::steady_clock::now();

        PAPI_accum(eventset, &count);
        count = 0;

        for (int i = 0; i < R; ++i) eng.apply(q.first, q.second, F);

        PAPI_accum(eventset, &count);
        std::cout << count << std::endl;

        // auto t1 = std::chrono::steady_clock::now();
        // std::cout << (static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count()) / R) << std::endl;
    }

    PAPI_stop(eventset, &count);

    return 0;
} // main
