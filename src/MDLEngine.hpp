/***
 *  $Id$
 **
 *  File: MDLEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MDL_ENGINE_HPP
#define MDL_ENGINE_HPP

#include "MDL.hpp"
#include "Priors.hpp"
#include "ScoringEngine.hpp"
#include "limits.hpp"
#include "log.hpp"

#include <jaz/timer.hpp>


template <int N, typename CountingQueryEngine>
class MDLEngine : public ScoringEngine<N> {
public:
    using set_type = uint_type<N>;
    using score_type = MDL::score_type;

    MDLEngine(CountingQueryEngine& cqe, MDL& mdl, const Priors<N>& priors, int ps = -1)
        : ScoringEngine<N>(ps), cqe_(cqe), mdl_(mdl), priors_(priors) {
        Log.debug() << "building MDLEngine..." << std::endl;

        n_ = cqe_.n();
        m_ = cqe_.m();

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        H_.resize(n_, -1);
        md_.resize(n_ + 1, E_);
        md_[0] = X_;
        norder_.resize(n_, -1);

        Log.debug() << "MDLEngine getting entropies..." << std::endl;
        m_init_entropy__();

        if (cqe_.is_reorderable()) m_reorder_counting_query_engine__();
        m_max_pa_size__();

        Log.debug() << "MDLEngine ready!" << std::endl;
    } // MDLEngine

    MDLEngine(CountingQueryEngine& cqe, MDL& mdl, int ps = -1)
        : MDLEngine(cqe, mdl, Priors<N>(), ps) { }

    int n() const { return n_; }

    int m() const { return m_; }

    bool process(const set_type& pa, set_type& ch, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer, int l) {
        set_type ch_ext = set_empty<set_type>();

        // ok, we have priors to process
        if (!priors_.empty()) {
            for (int xi = 0; xi < n_; ++xi) {
                if (!in_set(ch, xi)) continue;
                auto action = priors_.check(xi, pa);
                // IGNORE means pa and its supersets violate prior
                // EXTEND means pa violates prios, but its supersets may be ok
                if (action == priors_type::IGNORE) ch = set_remove(ch, xi);
                else if (action == priors_type::EXTEND) {
                    ch = set_remove(ch, xi);
                    ch_ext = set_add(ch_ext, xi);
                }
            }
        } // if priors_

        //std::vector<MDL> vec_mdl(set_size(ch), mdl_);
        //cqe_.apply(ch, pa, vec_mdl);

        std::vector<MDL> vec_mdl(1, mdl_);

        jaz::timer jt;

        for (int xi = 0, idx = 0; xi < n_; ++xi) {
            if (!in_set(ch, xi)) continue;

            jt.reset();
            cqe_.apply(set_add(E_, xi), pa, vec_mdl);
            query_time_ += jt.elapsed();

            //if (!m_extend_insert__(xi, pa, vec_mdl[idx].score(), mps_list, mps_buffer)) ch = set_remove(ch, xi);
            if (!m_extend_insert__(xi, pa, vec_mdl[0].score(), mps_list, mps_buffer)) ch = set_remove(ch, xi);
            ++idx;
        }

        // if we EXTEND then MPS may not be able to answer all queries d()
        // we fix it by inserting emptyset with infinity
        if (!is_emptyset(ch_ext) && is_emptyset(pa)) {
            for (int xi = 0; xi < n_; ++xi) {
                if (in_set(ch_ext, xi)) mps_buffer.push_back({xi, {SABNA_DBL_INFTY, pa}});
            }
        }

        ch = ch | ch_ext;
        ch = ch & md_[l + 1];

        return true;
    } // process

    void finalize(MPSList<N>& mps_list) {
        // if we have too crazy priors (too many parents)
        for (int xi = 0; xi < n_; ++xi) {
            if (mps_list.max_pa_size(xi) < priors_.max_pa_size(xi)) {
                Log.warn() << "prior required too many parents, most likely incorrect MPS" << std::endl;
                break;
            }
        }
        if (is_reordered) mps_list.map_variables(norder_);
        Log.info() << "total time in processing queries: " << query_time_ << "ns" << std::endl;
    } // finalize


private:
    inline static double query_time_ = 0;

    void m_init_entropy__() {
        std::vector<MDL> vec_mdl(1, mdl_);
        for (int xi = 0; xi < n_; ++xi) {
            cqe_.apply(set_add(E_, xi), set_remove(X_, xi), vec_mdl);
            auto res = vec_mdl[0].score();
            H_[xi] = res.second;
        }
    } // m_init_entropy__

    bool m_extend_insert__(int xi, const set_type& pa, const MDL::score_type& score, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer) {
        auto res = mps_list.find(xi, pa);
        if (score.first < res.s) mps_buffer.push_back({xi, {score.first, pa}});
        else if (score.first - score.second + H_[xi] >= res.s) return false;
        return true;
    } // m_extend_insert__

    void m_reorder_counting_query_engine__() {
        struct PNode {
            int xi;
            int ri;
            double H;

            bool operator<(const PNode& rhs) const {
                if (ri != rhs.ri) return ri > rhs.ri;
                return H < rhs.H;
            }
        }; // struct PNode

        std::vector<PNode> v(n_);
        std::vector<MDL> vec_mdl;
        vec_mdl.resize(1, mdl_);

        for (int xi = 0; xi < n_; ++xi) {
            cqe_.apply(set_add(E_, xi), set_remove(X_, xi), vec_mdl);
            auto res = vec_mdl[0].score();
            v[xi] = PNode{xi, cqe_.r(xi), res.second};
        }

        std::sort(std::begin(v), std::end(v));

        for (int i = 0; i < n_; ++i) {
            norder_[i] = v[i].xi;
            H_[i] = v[i].H;
        }

        is_reordered = cqe_.reorder(norder_);

        if (is_reordered) {
            Log.debug() << "variables reordered for efficiency" << std::endl;
            priors_.reorder(norder_);
        }
    } // m_reorder_counting_query_engine__

    void m_max_pa_size__() {
        std::vector<MDL> vec_mdl(n_, mdl_);
        cqe_.apply(X_, E_, vec_mdl);

        std::vector<std::pair<int, int>> vec_xi_r(n_, {0, 0});
        for (int xi = 0; xi < n_; ++xi) { vec_xi_r[xi] = {xi, cqe_.r(xi)}; }

        std::sort(vec_xi_r.begin(), vec_xi_r.end(),
                  [] (const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
                      if (lhs.second == rhs.second) { return lhs.first < rhs.first;  }
                      return lhs.second < rhs.second;
                  });

        if ((this->pa_size < 1) || (this->pa_size > n_ - 1)) this->pa_size = n_;
        Log.info() << "maximal number of parents limited to " << this->pa_size << std::endl;

        for (int xi = 0; xi < n_; ++xi) {
            double thres = vec_mdl[xi].score().first - H_[xi];
            double val = vec_mdl[xi].score().first - vec_mdl[xi].score().second;

            for (int j = 0, l = 0; (j < n_) && (l < this->pa_size); ++j) {
                int xj = vec_xi_r[j].first;
                int r_xj = vec_xi_r[j].second;
                if (xi != xj) {
                    val *= r_xj;
                    if (val >= thres) break;
                    ++l;
                    md_[l] = set_add(md_[l], xi);
                }
            } // for j
        } // for xi

     } // m_max_pa_size__

    int n_ = -1;
    int m_ = -1;

    CountingQueryEngine& cqe_;
    MDL mdl_;

    using priors_type = Priors<N>;
    const priors_type& priors_;

    set_type X_;
    set_type E_;

    bool is_reordered = false;

    std::vector<double> H_;
    std::vector<int> norder_;
    std::vector<set_type> md_;

}; // class MDLEngine

#endif // MDL_ENGINE_HPP
