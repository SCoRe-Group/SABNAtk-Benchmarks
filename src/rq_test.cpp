#include <fstream>
#include <iostream>
#include <vector>

#include <jaz/hash.hpp>

#include <bit_util.hpp>
#include <sabnatk.hpp>

#include "ADTree.hpp"
#include "csv.hpp"
#include "read_queries.hpp"


struct kv_t {
    int k;
    int v;
}; // struct kv_t

inline bool operator<(const kv_t& lhs, const kv_t& rhs) {
    return (lhs.k < rhs.k) || (!(rhs.k < lhs.k) && (lhs.v < rhs.v));
} // operator<

template <bool DeepCheck = false> class check_estimator {
public:
    void init(int ri, int qi) {
        state_.clear();
    } // init

    void finalize(int qi) {
        std::sort(std::begin(state_), std::end(state_));
        qi_ = qi;
    } // finalize

    void operator()(int Nij) { state_.push_back({-Nij, Nij}); }

    void operator()(int Nijk, int Nij) { state_.push_back({Nij, Nijk}); }

    void state() const {
        // some counters do not support qi_
        // std::cout << qi_;

        if constexpr (DeepCheck) {
            for (auto& x : state_) std::cout << " " << x.k << " " << x.v;
        } else {
            std::cout << " " << H_(reinterpret_cast<const char*>(state_.data()), state_.size() * sizeof(kv_t));
        }

        std::cout << std::endl;
    } // state

private:
    std::vector<kv_t> state_;
    int qi_ = 0;

    jaz::murmur64A H_;

}; // class check_estimator


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "usage: rq_test csv_file dat_file" << std::endl;
        return -1;
    }

    const int N = 1;

    using set_type = uint_type<N>;
    using data_type = uint8_t;

    std::vector<data_type> D;

    auto [res, n, m] = read_csv(argv[1], D);

    if (res == false) {
        std::cout << "error: unable to read input data" << std::endl;
        return -1;
    }

    std::vector<std::pair<set_type, set_type>> Q;

    if (!read_queries(argv[2], Q)) {
        std::cout << "error: unable to read queries" << std::endl;
        return -1;
    }


    // CREATE ENGINE
    //ADTree<N, data_type> eng(16, n, m, D);
    //eng.build_tree();

    auto eng = sabnatk::create_counter<N, sabnatk::Rad>(n, m, std::begin(D));


    // RUN TEST
    // changing deep_check to true will provide
    // the actual count gathered during the execution
    // instead of the fingerprint
    const bool deep_check = false;
    std::vector<check_estimator<deep_check>> F(1);

    for (const auto& q : Q) {
        eng.apply(q.first, q.second, F);
        F[0].state();
    }

    return 0;
} // main
