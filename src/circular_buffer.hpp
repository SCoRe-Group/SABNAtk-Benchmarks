#ifndef CIRCULAR_BUFFER_HPP
#define CIRCULAR_BUFFER_HPP

#include <mutex>
#include <vector>


// Class: circular_block_buffer
// Implementation of a circular buffer in which elements
// are stored in blocks, that can be locked
// for processing in multi-threaded environment
//
// Parameters:
// T - type of objects stored in the buffer
// N - capacity of the buffer
template <typename T, int N>
class circular_block_buffer {
public:
    using value_type = T;

    // Class: accessor_type
    // Abstraction to simplify locking/unlocking of the buffers
    // managed by the circular_block_buffer.
    class accessor_type {
    public:
        accessor_type(circular_block_buffer* addr, int pos, bool hard = false)
            : addr_(addr), pos_(pos), hard_(hard) {
            addr->mtx_[pos].lock();
        } // accessor_type

        std::vector<T>& operator*() { return addr_->buffer_[pos_]; }

        std::vector<T>* operator->() { return &addr_->buffer_[pos_]; }

        ~accessor_type() {
            if (hard_) addr_->buffer_[pos_].clear();
            addr_->mtx_[pos_].unlock();
        } // ~accessor_type

    private:
        circular_block_buffer* addr_;
        int pos_;
        bool hard_;

    }; // class accessor_type


    // Constructor: circular_block_buffer
    //
    // Parameters:
    // bs - block size.
    explicit circular_block_buffer(int bs = 1): bs_(bs) { }


    // Function: set_block_size
    // Changes the underlying block size.
    // Currently not thread-safe.
    //
    // Parameters:
    // bs - new block size.
    void set_block_size(int bs) { bs_ = bs; }

    // Function: get_block_size
    // Currently not thread-safe.
    //
    // Returns: the current block size.
    int get_block_size() const { return bs_; }


    // Function: put
    // Adds element to the buffer. Put can be called only by a single thread.
    // However, it can be called concurrently with _get methods.
    //
    // Parameters:
    // t - element to add to the buffer.
    //
    // Returns: index and the size of the block that has been filled and can be processed
    // or tuple (-1, 0) if adding element did not fill up a block. The size of the block
    // is not guaranteed to be the same by the time block is accessed via accesor.
    std::pair<int, int> put(const T& t) {
        const std::lock_guard<std::mutex> lock(mtx_[head_]);
        buffer_[head_].push_back(t);
        return m_make_put__();
    } // put

    std::pair<int, int> put(T&& t) {
        const std::lock_guard<std::mutex> lock(mtx_[head_]);
        buffer_[head_].push_back(std::move(t));
        return m_make_put__();
    } // put

    template <typename... Args>
    std::pair<int, int> put(Args&&... args) {
        const std::lock_guard<std::mutex> lock(mtx_[head_]);
        buffer_[head_].emplace_back(args...);
        return m_make_put__();
    } // put


    // Function: put
    // Forces the buffer to advance to the next block,
    // and returns index of the current block (even if it is not full).
    // Put can be called only by a single thread.
    // However, it can be called concurrently with _get methods.
    //
    // Returns: position and the size of the current block if it is not empty,
    // and tuple (-1, 0) otherwise. The size of the block is not guaranteed to
    // be the same by the time block is accessed via accesor.
    std::pair<int, int> put() {
        const std::lock_guard<std::mutex> lock(mtx_[head_]);
        auto bs = buffer_[head_].size();
        if ((bs > 0) && (bs < bs_)) {
            int rv = head_;
            head_ = (head_ + 1) % N;
            return {rv, buffer_[rv].size()};
        }
        return {-1, 0};
    } // put

    // Function: soft_get
    // Creates accessor to the block stored on position *pos*.
    // The accessor will lock the block for the accessor's duration.
    // This is entirely thread safe way to access block.
    //
    // Parameters:
    // pos - position of the block to lock.
    //
    // Returns: accessor that can be used to access the block.
    accessor_type soft_get(int pos) { return accessor_type(this, pos); }

    // Function: hard_get
    // Creates accessor to the block stored on position *pos*.
    // The accessor will lock the block for the accessor's duration,
    // and will clear the block when leaving the scope.
    // This is entirely thread safe way to access block.
    //
    // Parameters:
    // pos - position of the block to lock.
    //
    // Returns: accessor that can be used to access the block.
    accessor_type hard_get(int pos) { return accessor_type(this, pos, true); }


private:
    std::pair<int, int> m_make_put__() {
        if (buffer_[head_].size() < bs_) return {-1, 0};

        // if size of the block is == bs_ we have new block to process
        // so we should advance head_ and release the block
        // otherwise, we overflowed already released block
        // hence we should only advance head and do not release
        int rv = head_;
        head_ = (head_ + 1) % N;

        if (buffer_[rv].size() == bs_)  return { rv, buffer_[rv].size() };

        return {-1, 0};
    } // m_make_put__

    int bs_ = 1;
    int head_ = 0;

    std::vector<value_type> buffer_[N];
    std::mutex mtx_[N];

}; // class circular_block_buffer

#endif // CIRCULAR_BUFFER_HPP
