/***
 *  $Id$
 **
 *  File: MPSBuild.hpp
 *  Created: June 4, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MPS_BUILD_HPP
#define MPS_BUILD_HPP

#include <algorithm>
#include <string>
#include <vector>

#include <tbb/enumerable_thread_specific.h>
#include <tbb/task_group.h>

#include <jaz/algorithm.hpp>

#include "MPS.hpp"
#include "log.hpp"


template <int N> class MPSBuild : public MPS<N> {
public:
    using set_type = typename MPS<N>::set_type;

    struct Task {
        set_type pa_;
        set_type ch_;
    }; // struct Task


    using mps_node_t = std::pair<int, typename MPS<N>::MPSNode>;
    using shared_mps_t = tbb::enumerable_thread_specific<std::vector<mps_node_t>>;
    using shared_tasks_t = tbb::enumerable_thread_specific<std::vector<Task>>;


    template <typename Engine> void run(Engine score, int min_dfs) {
        n_ = score.n();
        MPS<N>::init(n_);

        if (min_dfs < 1) min_dfs = n_ + 1;

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        shared_tasks_t cl, nl;
        shared_mps_t mps_buffer;

        auto& nlt = nl.local();
        nlt.push_back({E_, X_});

        long long int nl_size = 1;

        tbb::task_group tg;

        for (int l = 0; (l < n_) && (nl_size > 0) && (l <= min_dfs); ++l) {
            auto start = std::chrono::system_clock::now();

            std::swap(cl, nl);
            nl.clear();

            int cl_size = cl.size();

            if (l < min_dfs) {
                for (auto iter = cl.begin(); iter != cl.end(); ++iter) {
                    for (auto i = std::begin(*iter); i != std::end(*iter); ++i) {
                        tg.run([&, i] {
                                   auto& mps = mps_buffer.local();
                                   score.process(i->pa_, i->ch_, MPS<N>::mps_list, mps, l);

                                   auto& nlt = nl.local();
                                   if (!is_emptyset(i->ch_)) m_fill_next_layer__(i->pa_, i->ch_, nlt); });
                    } // for i
                } // for iter
            } else {
                Log.info() << "switching to DFS mode..." << std::endl;

                for (auto iter = cl.begin(); iter != cl.end(); ++iter) {
                    for (auto i = std::begin(*iter); i != std::end(*iter); ++i) {
                        tg.run([&, i] {
                                   auto& mps = mps_buffer.local();
                                   m_dfs__<Engine>(score, i->pa_, i->ch_, mps); });
                    } // for i
                } // for iter
            } // if l

            tg.wait();

            nl_size = 0;
            for (auto i = nl.begin(); i != nl.end(); ++i) nl_size += i->size();

            Log.debug() << "updating MPS..." << std::endl;

            int mps_buf_sz = 0;

            for (auto i = mps_buffer.begin(); i != mps_buffer.end(); ++i) {
                mps_buf_sz += i->size();
                tg.run([&, i] { std::sort(i->begin(), i->end(), [](const mps_node_t& x, const mps_node_t& y) {
                                return x.first < y.first; }); });
            }

            tg.wait();

            Log.debug() << "sort done, MPS update size is " << mps_buf_sz << std::endl;

            for (auto i = mps_buffer.begin(); i != mps_buffer.end(); ++i) {
                auto& mpsb = *i;

                auto first = mpsb.begin();
                auto last = mpsb.end();

                while (first != last) {
                    auto iter = jaz::range(first, last, [](const mps_node_t& x, const mps_node_t& y) {
                                                            return x.first == y.first; });
                    tg.run([&, first, iter] {
                               auto lf = first;
                               for (; lf != iter; ++lf) {
                                   this->mps_list.insert(lf->first, lf->second.pa, lf->second.s);
                               }
                           });

                    first = iter;
                }

                tg.wait();
                mpsb.clear();
            } // for i

            Log.debug() << "MPS updated" << std::endl;

            if (l >= min_dfs) {
                Log.debug() << "sanitizing MPS..." << std::endl;
                MPS<N>::mps_list.verify_rebuild();
                break;
            }

            std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
            Log.info() << "processed layer " << l << ", size of next layer " << nl_size << ", time: " << jaz::log::second_to_time(diff.count()) << std::endl;
            Log.debug() << "memory required: " << jaz::log::byte_to_size(nl_size * sizeof(Task)) << std::endl;
        } // for l

        score.finalize(MPS<N>::mps_list);
    } // run

private:
    void m_fill_next_layer__(const set_type& pa, const set_type& ch, std::vector<Task>& nl);
    void m_prune_next_layer__(const std::vector<Task>& B, std::vector<Task>& nl);

    template <typename Engine>
    void m_dfs__(Engine& score, set_type& pa, set_type& ch, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer) {
        struct stack_el {
            int xi_max;
            bool done;
            Task tk;
        };

        int xi_max = msb(pa);

        std::vector<stack_el> stack;
        stack.push_back({xi_max, false, {pa, ch}});

        while (!stack.empty()) {
            if (!stack.back().done) {
                score.process(stack.back().tk.pa_, stack.back().tk.ch_, MPS<N>::mps_list, mps_buffer, set_size(pa));
                stack.back().done = true;
            }

            set_type new_ch = set_empty<set_type>();
            bool to_pop = false;
            int xi = ++stack.back().xi_max;
            if (xi >= n_) { to_pop = true; }
            else { new_ch = set_remove(stack.back().tk.ch_, xi); }
            if (to_pop || is_emptyset(new_ch)) { stack.pop_back(); }
            else { stack.push_back({xi, false, {set_add(stack.back().tk.pa_, xi), new_ch}}); }
        }
    } // m_dfs__

    int n_;

    set_type X_;
    set_type E_;

}; // class MPSBuild

template <int N>
inline void MPSBuild<N>::m_fill_next_layer__(const set_type& pa, const set_type& ch, std::vector<Task>& nl) {
    int xi_max = msb(pa);

    for (int xi = xi_max + 1; xi < n_; ++xi) {
        set_type new_ch = set_remove(ch, xi);
        if (!is_emptyset(new_ch)) { nl.push_back({set_add(pa, xi), new_ch}); }
    }
} // m_fill_next_layer__

#endif // MPS_BUILD_HPP
