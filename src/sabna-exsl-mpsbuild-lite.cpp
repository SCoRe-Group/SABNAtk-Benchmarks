/***
 *  $Id$
 **
 *  File: sabna-exsl-mpsbuild.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017-2019 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <limits>
#include <vector>

#include <cxxopts/cxxopts.hpp>

#include <sabnatk.hpp>

#include "ADTree.hpp"
#include "Hasher.hpp"

#include "MDLEngine.hpp"
#include "MPSBuild.hpp"
#include "Priors.hpp"
#include "csv.hpp"
#include "log.hpp"


using data_type = uint8_t;


struct Functors {
    MDL mdl;
}; // struct Functors


enum l_counter_type { Rad = 0, BV = 1, CT = 2, Auto = 3, ADT = 4, HT = 5 };

template <int N, l_counter_type Type>
auto l_create_counter(int n, int m, std::vector<data_type>& D) {
    if constexpr (Type == BV) return sabnatk::create_BVCounter<N>(n, m, std::begin(D));
    if constexpr (Type == CT) return sabnatk::create_CTCounter<N>(n, m, std::begin(D));
    if constexpr (Type == Rad) return sabnatk::create_RadCounter<N>(n, m, std::begin(D));
    if constexpr (Type == Auto) return sabnatk::create_AutoCounter<N>(n, m, std::begin(D));
    if constexpr (Type == ADT) {
        ADTree<N, data_type> eng(16, n, m, D);
        eng.build_tree();
        return eng;
    }
    if constexpr (Type == HT) return create_Hasher<N, data_type>(n, m, std::begin(D));
} // l_create_counter


template <int N>
std::pair<bool, std::string> build_and_write(int n, int m, std::vector<data_type>& D, const std::string& mps_name, const std::string& priors_name, const std::string& scoring_f, Functors& func, const std::string& cqe_type, int pa_size, int min_dfs) {
    Log.debug() << "processing with N = " << N << std::endl;

    Priors<N> priors;

    if (!priors_name.empty()) {
        // Log.warn() << "support for priors is experimental, caution advised!" << std::endl;
        Log.debug() << "reading priors" << std::endl;
        auto res = priors.read(priors_name, n);
        if (!res.first) return res;
    }

    MPSBuild<N> sl_mps;

    auto start = std::chrono::system_clock::now();


    if (cqe_type == "auto") {
        Log.debug() << "building AutoCounter engine" << std::endl;

        const l_counter_type ct = Auto;
        auto ac = l_create_counter<N, ct>(n, m, D);

        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        sl_mps.run(MDLEngine<N, decltype(ac)>(ac, func.mdl, priors, pa_size), min_dfs);
    } else if (cqe_type == "rad") {
        Log.debug() << "building RadCounter engine" << std::endl;

        const l_counter_type ct = Rad;
        auto ac = l_create_counter<N, ct>(n, m, D);

        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        sl_mps.run(MDLEngine<N, decltype(ac)>(ac, func.mdl, priors, pa_size), min_dfs);
    } else if (cqe_type == "bv") {
        Log.debug() << "building BVCounter engine" << std::endl;

        const l_counter_type ct = BV;
        auto ac = l_create_counter<N, ct>(n, m, D);

        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        sl_mps.run(MDLEngine<N, decltype(ac)>(ac, func.mdl, priors, pa_size), min_dfs);
    } else if (cqe_type == "ct") {
        Log.debug() << "building CTCounter engine" << std::endl;

        const l_counter_type ct = CT;
        auto ac = l_create_counter<N, ct>(n, m, D);

        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        sl_mps.run(MDLEngine<N, decltype(ac)>(ac, func.mdl, priors, pa_size), min_dfs);
    } else if (cqe_type == "adt") {
        Log.debug() << "building ADTree engine, prepare for impact..." << std::endl;

        const l_counter_type ct = ADT;
        auto ac = l_create_counter<N, ct>(n, m, D);

        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        sl_mps.run(MDLEngine<N, decltype(ac)>(ac, func.mdl, priors, pa_size), min_dfs);
    } else if (cqe_type == "ht") {
        Log.debug() << "building Hasher engine" << std::endl;

        const l_counter_type ct = HT;
        auto ac = l_create_counter<N, ct>(n, m, D);

        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        sl_mps.run(MDLEngine<N, decltype(ac)>(ac, func.mdl, priors, pa_size), min_dfs);
    } else {
        return {false, "unknown to humanity engine requested :-("};
    }

    Log.info() << "writing MPS with " << sl_mps.size() << " parent sets" << std::endl;
    auto res = sl_mps.write(mps_name);

    return res;
} // build_and_write


int main (int argc, const char* argv[]) {
    InitLog();

    std::string csv_name;
    std::string mps_name;
    std::string priors_name;
    std::string scoring_f;
    std::string cqe_type;
    int pa_size;
    int min_dfs;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(csv_name))
            ("mps-file", "output mps file", cxxopts::value<std::string>(mps_name))
            ("priors-file", "hard priors file", cxxopts::value<std::string>(priors_name)->default_value(""))
            ("cqe", "counting query engine [auto|rad|bvc|ct|adt|ht]", cxxopts::value<std::string>(cqe_type)->default_value("auto"))
            ("l,pa-size", "parent set size limit", cxxopts::value<int>(pa_size)->default_value("-1"))
            ("d,dfs-level", "level to switch to DFS", cxxopts::value<int>(min_dfs)->default_value("-1"))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("csv-file") && opt_res.count("mps-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (!opt_res.unmatched().empty()) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }

    std::vector<data_type> D;
    auto [b, n, m] = read_csv(csv_name, D);

    if (b == false) {
        Log.error() << "could not read input data" << std::endl;
        return -1;
    }

    Log.info() << "input: " << csv_name << std::endl;
    Log.info() << "variables: " << n << std::endl;
    Log.info() << "instances: " << m << std::endl;
    Log.info() << "function: " << scoring_f << std::endl;
    Log.info() << "limit: " << pa_size << std::endl;
    Log.info() << "priors: " << priors_name << std::endl;
    Log.info() << "output: " << mps_name << std::endl;

    Log.info() << "searching parent sets" << std::endl;
    auto start = std::chrono::system_clock::now();

    // initialize functors based on provided and default hyper-parameters
    scoring_f = "mdl";
    Functors func;
    func.mdl = MDL(m);

    std::pair<bool, std::string> res;

    if (n <= set_max_item<1>())      res = build_and_write<1>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n <= set_max_item<2>()) res = build_and_write<2>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n <= set_max_item<3>()) res = build_and_write<3>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n <= set_max_item<4>()) res = build_and_write<4>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else {
        Log.error() << "unable to handle more than " << set_max_item<4>() << " variables!";
        return -1;
    }

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
